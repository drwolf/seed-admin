package schedulers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import model.Device;
import model.Site;
import model.Site.SiteStatus;
import model.StatusData;
import play.Logger;
import play.db.jpa.JPA;
import play.libs.Akka;
import play.libs.F.Callback0;
import scala.concurrent.duration.Duration;
import akka.actor.Scheduler;

public class BatteryStatusScheduler {

	public static void dailyScheduler() {
		Long delayInSeconds = 0L;
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 3);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		Date plannedStart = c.getTime();
		Date now = new Date();
		Date nextRun;
		if (now.after(plannedStart)) {
			c.add(Calendar.DAY_OF_WEEK, 1);
			nextRun = c.getTime();
		} else {
			nextRun = c.getTime();
		}
		delayInSeconds = (nextRun.getTime() - now.getTime()) / 1000;
		delayInSeconds = new Long(60);
		Scheduler scheduler = Akka.system().scheduler();
		scheduler.schedule(Duration.create(delayInSeconds, TimeUnit.SECONDS),
				Duration.create(60, TimeUnit.MINUTES), new Runnable() {
					List<Site> sites = new ArrayList<Site>();

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Logger.info("parte lo scheduler Daily BatteryStatus");

						// prendo tutti i siti che stanno registrando
						JPA.withTransaction(new Callback0() {
							@SuppressWarnings("unchecked")
							public void esegui() {
								sites = JPA
										.em()
										.createQuery(
												"from Site where status = :s")
										.setParameter("s", SiteStatus.RECORDING)
										.getResultList();
								for (Site site : sites) {
									site.getMasters();
								}
							}

							@Override
							public void invoke() throws Throwable {
								this.esegui();
							}
						});
						if (this.sites.size() > 0) {
							for (Site site : this.sites) {
								for (final Device master : site.getMasters()) {
									// per ogni master prendo l'ultimo valore
									// letto dello status
									JPA.withTransaction(new Callback0() {
										@SuppressWarnings("unchecked")
										public void esegui() {
											List<StatusData> statusData = JPA
													.em()
													.createQuery(
															"from StatusData where nodeDest = :nd order by data desc")
													.setParameter("nd", master)
													.setMaxResults(1)
													.getResultList();
											if (statusData.size() > 0) {
												StatusData sd = statusData
														.get(0);
												if (sd.getBatteryVoltage()
														.compareTo(
																BatteryStatusScheduler.BATTERY_100) > 0) {
													master.setBatteryIndicator(3);
												} else if (sd
														.getBatteryVoltage()
														.compareTo(
																BatteryStatusScheduler.BATTERY_60) > 0) {
													master.setBatteryIndicator(2);
												} else if (sd
														.getBatteryVoltage()
														.compareTo(
																BatteryStatusScheduler.BATTERY_THRESHOLD) > 0) {
													master.setBatteryIndicator(1);
												} else {
													master.setBatteryIndicator(0);
												}
												JPA.em().merge(master);
											}
										}

										@Override
										public void invoke() throws Throwable {
											this.esegui();
										}
									});
								}
							}
						}
						Logger.info("Termine Daily BatteryStatus");
					}

				}, Akka.system().dispatcher());
	}

	public static void hourlyScheduler() {
		Date now = new Date();
		Long delayInSeconds;
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		c.set(Calendar.MINUTE, 53);
		if (now.before(c.getTime())) {
			c.add(Calendar.HOUR, 1);
		}
		Date nextRun = c.getTime();
		delayInSeconds = (nextRun.getTime() - now.getTime()) / 1000;
		Scheduler scheduler = Akka.system().scheduler();
		scheduler.schedule(Duration.create(delayInSeconds, TimeUnit.SECONDS),
				Duration.create(60, TimeUnit.MINUTES), new Runnable() {
			List<Site> sites = new ArrayList<Site>();

			@Override
			public void run() {
				Logger.info("parte lo scheduler Hourly BatteryStatus");

						// prendo tutti i siti che stanno registrando
				JPA.withTransaction(new Callback0() {
					@SuppressWarnings("unchecked")
					public void esegui() {
						sites = JPA
								.em()
								.createQuery(
										"from Site where status = :s")
										.setParameter("s", SiteStatus.RECORDING)
										.getResultList();
						for (Site site : sites) {
							site.getMasters();
						}
					}

					@Override
					public void invoke() throws Throwable {
						this.esegui();
					}
				});
				if (this.sites.size() > 0) {
					for (Site site : this.sites) {
						for (final Device master : site.getMasters()) {
							// per ogni master prendo l'ultimo valore
							// letto dello status
							JPA.withTransaction(new Callback0() {
								@SuppressWarnings("unchecked")
								public void esegui() {
											List<StatusData> statusData = JPA
											.em()
											.createQuery(
													"from StatusData where nodeDest = :nd order by data desc")
													.setParameter("nd", master)
													.setMaxResults(1)
													.getResultList();
									if (statusData.size() > 0) {
										StatusData sd = statusData
												.get(0);
										if (sd.getBatteryVoltage()
												.compareTo(
														BatteryStatusScheduler.BATTERY_THRESHOLD) <= 0) {
											master.setBatteryIndicator(0);
											JPA.em().merge(master);
										}
									}
								}

								@Override
								public void invoke() throws Throwable {
									this.esegui();
								}
							});
						}
					}
				}
				Logger.info("Termine Hourly BatteryStatus");
			}

		}, Akka.system().dispatcher());
	}

	private final static BigDecimal BATTERY_THRESHOLD = new BigDecimal(23.2);

	private final static BigDecimal BATTERY_60 = new BigDecimal(24.5);

	private final static BigDecimal BATTERY_100 = new BigDecimal(25.5);

}
