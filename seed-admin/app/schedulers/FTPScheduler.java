package schedulers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ftp.FTPConnectionManager;
import model.Device;
import model.Site;
import model.Site.SiteStatus;
import play.Logger;
import play.db.jpa.JPA;
import play.libs.Akka;
import play.libs.F.Callback0;
import scala.concurrent.duration.Duration;
import akka.actor.Scheduler;

public class FTPScheduler {

	public static void scheduler() {
		// Lo scheduler deve girare a 51 di ogni ora.
		// devo calcolare il delay tra l'istante in cui parte e il minuti 51
		// dell'ora.
		Date now = new Date();
		Long delayInSeconds;
		Calendar c = Calendar.getInstance();
		// imposto l'ora corrente
		c.setTime(now);
		// sposto i minuti al 53 (a seguti dell'aggiunto degli scanning a fine
		// di ogni ora)
		c.set(Calendar.MINUTE, 53);
		if (now.before(c.getTime())) {
			// se l'ora attuale è prima del 51 aggiungo un ora
			// in questo modo sono sicuro che la prima schedulazione trovi dei
			// dati.
			c.add(Calendar.HOUR, 1);
		}
		// imposto il nextRun con la nuova ora
		Date nextRun = c.getTime();
		delayInSeconds = (nextRun.getTime() - now.getTime()) / 1000;
		Logger.debug("Calcolato il delay del cron in secondi : "
				+ delayInSeconds.toString());
		// ora imposto lo scheduler con il delay calcolato e con frequenza
		// oraria
		// mettto delay a 2 minuti per debug
		// delayInSeconds = new Long(60);
		Scheduler scheduler = Akka.system().scheduler();
		scheduler.schedule(Duration.create(delayInSeconds, TimeUnit.SECONDS),
				Duration.create(60, TimeUnit.MINUTES), new Runnable() {
					List<Site> sites = new ArrayList<Site>();

					@Override
					public void run() {
						Logger.info("parte lo scheduler");
						// prendo tutti i siti che stanno registrando
						JPA.withTransaction(new Callback0() {
							@SuppressWarnings("unchecked")
							public void esegui() {
								sites = JPA
										.em()
										.createQuery(
												"from Site where status = :s")
										.setParameter("s", SiteStatus.RECORDING)
										.getResultList();
								for (Site site : sites) {
									site.getMasters();
								}
							}

							@Override
							public void invoke() throws Throwable {
								this.esegui();
							}
						});
						FTPConnectionManager ftpManager = new FTPConnectionManager();
						if (this.sites.size() > 0) {
							for (Site site : this.sites) {
								Logger.debug("Raccolgo i dati di "
										+ site.getName());
								// per ogni master del sito in
								// monitoraggio devo scaricare i
								// dati dall'area ftp
								// Itero due vole, nella prima controllo
								// che siano presenti dati sul master e
								// poi gli carico il GetFile
								// Mi creo una lista dove metto solo
								// quelli dove ho caricato il getFiles
								ArrayList<Device> daScaricare = new ArrayList<Device>();
								for (Device master : site.getMasters()) {
									try {
										if (ftpManager.isDataExist(master)) {
											// se sono presenti dati
											// procedo nello scaricarli
											Logger.debug("Trovati dati da scaricare nel master "
													+ master.toString());
											// prinma di inviare il GetFile
											// dovrei verificare che non sia per
											// qualche motivo il Data_Read sul
											// server al passaggio precedente.
											// Nel caso succeddesse il master
											// sovrascriverebbe il file presente
											Logger.debug("prima di caricare il GetFile verifico che non ci siano dati rimastri sul server ftp");
											ftpManager.saveDataRead(master);
											ftpManager.sendGetFiles(master);
											daScaricare.add(master);

										}
									} catch (IOException e) {
										Logger.warn("Errore di connessione nel processo di richiesta dei dati con il device "
												+ master.toString());
										Logger.warn(e.toString());
									} catch (Exception e) {
										Logger.warn("Errore imprevisto nel processo di richiesta dei dati con il device "
												+ master.toString());
										Logger.warn(e.toString());
									}
								}
								// per essere sicuro che i dati arrivino sul
								// server FTP metto uno sleep
								Logger.debug("Vado a dormire per "
										+ String.valueOf(FTPScheduler.SLEEP)
								+ " millisecondi");
								try {
									Thread.sleep(FTPScheduler.SLEEP);
								} catch (InterruptedException e) {
								}
								// Nel seconso step itero su quelli in
								// cui cui ho caricato il getFiles e
								// prelveo dal server i dati
								// memorizzandoli su db
								for (Device device : daScaricare) {
									try {
										Logger.debug("Scarico i dati dal device "
												+ device.toString());
										ftpManager.saveDataRead(device);
									} catch (IOException e) {
										Logger.warn("Errore nella connessione nel processo di salvataggio dei dati dal device "
												+ device.toString());
										Logger.warn(e.toString());
									} catch (Exception e) {
										Logger.warn("Errore imprevisto nel processo di salvataggio dei dati dal device "
												+ device.toString());
										Logger.warn(e.toString());
									}

								}
							}
						} else {
							Logger.debug("Non sono stati trovati siti in registrazione");
						}
						Logger.info("Termine Scheduler");
					}
				}, Akka.system().dispatcher());
	}

	private final static int SLEEP = 300000; // 5 minuti
}
