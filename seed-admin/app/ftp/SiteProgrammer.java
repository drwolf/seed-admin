package ftp;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.Device;
import model.ScanningData;
import play.Logger;
import play.db.jpa.JPA;
import ftp.model.ProgramScanning;

public class SiteProgrammer {

	private final static String PROGRAM_WRITE_TEMPLATE_STRING = "Program_write_0%s.csv";

	public List<Device> scanningFromDB(Device device) {
		List<Device> result = new ArrayList<Device>();
		List<ScanningData> list = JPA
				.em()
				.createQuery(
						"from ScanningData where nodeDest = :d order by data desc")
						.setParameter("d", device).getResultList();
		if (list != null && list.size() > 0) {
			ScanningData sd = list.get(0);
			// Mi prendo la lista degli id in modo da poterla ordinare
			List<Long> ids = new ArrayList<Long>();
			for (Device d : device.getSite().getDevices()) {
				ids.add(d.getId());
			}
			// dopo l'ordinamento il primo elemento dovrebbe essere l'id più
			// basso e l'ultimo quello più alto
			Collections.sort(ids);
			// nel caso in cui debba prelevare lo scanning dal db devo assumere
			// che
			// la richiesta sia stata fatta chiedendo come nodoStart quello con
			// id
			// più basso e quello con nodoEnd quello con id più alto
			String[] res = sd.getResult().split(";");
			for (int j = 0; j < ids.size(); j++) {
				if (res[j].equals("1")) {
					// elemento visibile
					Device d = JPA.em().find(Device.class, ids.get(j));
					result.add(d);
				} else {
					Logger.info("elemento non visibile");
				}
			}
		}
		return result;
	}

	public List<Device> scanningFromFTP(Device device) throws SocketException,
			IOException {
		List<Device> result = new ArrayList<Device>();

		FTPConnectionManager ftpCM = new FTPConnectionManager();
		Logger.debug("Inizio la prodecura di scanning. Device: "
				+ device.getId());
		// Mi prendo la lista degli id in modo da poterla ordinare
		List<Long> ids = new ArrayList<Long>();
		for (Device d : device.getSite().getDevices()) {
			ids.add(d.getId());
		}
		// dopo l'ordinamento il primo elemento dovrebbe essere l'id più
		// basso e l'ultimo quello più alto
		Collections.sort(ids);
		List<ProgramScanning> elements = new ArrayList<ProgramScanning>();
		ProgramScanning ps = new ProgramScanning(-10, -1, device.getId(),
				ids.get(0), ids.get(ids.size() - 1));
		elements.add(ps);
		// invio la programmazione contente la richiesta di scanning al
		// concentratore
		Logger.debug("Invio il file di programmazione con la richiesta di scanning:");
		Logger.debug(elements.toString());
		ftpCM.sendProgramFile(device, elements);

		// a questo punto sono abbastanza sicuro di avere caricato
		// correttamente la schedulazione. Rimane il caso in cui il file di
		// programmaizone non sia corretto. In questo caso il file viene
		// digerito ma nessuna operazione viene eseguita

		// ora mi devo collegare al server ftp centrale e, se presente, devo
		// scaricare e salvare il contenuto del Data_read
		// una volta termianto devo caricare il file Get_Files e poi
		// aspettare il nuovo Data_read
		// devo collegarmi al server ftp del PLC
		Logger.debug("Scarico eventuali dati già presenti nel server");
		ftpCM.saveDataRead(device);

		// butto sul plc il file che mi fa scaricare i dati
		Logger.debug("Richiedo i nuovi dati al concentratore");
		ftpCM.sendGetFiles(device);

		Logger.debug("Elaboro il risultato");
		result = ftpCM.getScaningResult(device, ids);

		return result;
	}
}
