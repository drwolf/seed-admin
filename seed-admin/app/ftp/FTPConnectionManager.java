package ftp;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.SocketException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import model.Device;
import model.DynamicData;
import model.DynamicValues;
import model.ScanningData;
import model.StaticData;
import model.StaticData.DataType;
import model.StaticValues;
import model.StatusData;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import play.Logger;
import play.db.jpa.JPA;
import play.libs.F.Callback0;
import play.libs.F.Function0;
import au.com.bytecode.opencsv.CSVReader;
import ftp.model.ProgramIF;

public class FTPConnectionManager {

	private final static String PROGRAM_WRITE_TEMPLATE_STRING = "Program_write_0%s.csv";

	private final static String DATA_READ_TEMPLATE_STRING = "Data_read_0%s.csv";

	private final static String GET_FILE_TEMPLATE_STRING = "Get_Files_0%s.csv";

	private final static String IP_ADDRESS_FTP_MASTER_SERVER = "192.168.90.13";

	private final static String USERNAME_FTP_MASTER_SERVER = "seed";

	private final static String PASSWORD_FTP_MASTER_SERVER = "seed";
	private final static SimpleDateFormat FILE_DATE_FORMATTER = new SimpleDateFormat(
			"yyyy-MM-dd_HH:mm:ss");

	private final static SimpleDateFormat SQL_DATE_FORMATTER = new SimpleDateFormat(
			"yyyy-MM-dd-HH:mm:ss");

	private final static int MAX_CYCLE_COUNT = 20;

	private final static int WHILE_SLEEEP = 1000;

	private final static String PATH_DATA_READ_FILE = "backupDataRead/";

	private Device findDeviceById(final Long id) {

		Function0<Device> function0 = new Function0<Device>() {
			@Override
			public Device apply() throws Throwable {
				// TODO Auto-generated method stub
				return JPA.em().find(Device.class, id);
			}
		};

		try {
			JPA.em();
			try {
				return function0.apply();
			} catch (Throwable e) {
				Logger.error("Errore nel find del device con id " + id);
			}
		} catch (Exception e) {
			try {
				return JPA.withTransaction(function0);
			} catch (Throwable e1) {
				Logger.error("Errore nel find del device con id " + id);
			}
		}
		return null;
	}

	public List<Device> getScaningResult(Device device, List<Long> ids)
			throws SocketException, IOException {
		List<Device> result = new ArrayList<Device>();
		if (device.getConcentrator() != null) {
			FTPClient fs = new FTPClient();
			fs.connect(FTPConnectionManager.IP_ADDRESS_FTP_MASTER_SERVER);
			fs.login(FTPConnectionManager.USERNAME_FTP_MASTER_SERVER,
					FTPConnectionManager.PASSWORD_FTP_MASTER_SERVER);
			if (!FTPReply.isPositiveCompletion(fs.getReplyCode())) {
				Logger.debug("Exception in conneting to FTP Server "
						+ FTPConnectionManager.IP_ADDRESS_FTP_MASTER_SERVER);
				fs.disconnect();
			} else {
				Logger.debug("Succesfully connected to "
						+ FTPConnectionManager.IP_ADDRESS_FTP_MASTER_SERVER);
			}
			String drFileName = String.format(
					FTPConnectionManager.DATA_READ_TEMPLATE_STRING,
					device.getId());
			boolean flag = false;
			int cycleCount = 0;
			while (!flag && cycleCount < FTPConnectionManager.MAX_CYCLE_COUNT) {
				FTPFile[] files = fs.listFiles();
				for (FTPFile ftpFile : files) {
					if (ftpFile.getName().toUpperCase()
							.equals(drFileName.toUpperCase())) {
						Logger.info("trovato data read " + ftpFile.getName()
								+ " nel server lo scarico");
						flag = true;
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						fs.retrieveFile(ftpFile.getName(), baos);

						BufferedReader bufferedReader = new BufferedReader(
								new InputStreamReader(new ByteArrayInputStream(
										baos.toByteArray())));
						CSVReader reader = new CSVReader(bufferedReader, ';');
						List<String[]> lines = reader.readAll();
						for (int i = lines.size() - 1; i > 0; i--) {
							// leggo il file partendo dall'ultima riga dato che
							// mi aspetto che l'operaiozne che cerco sia la più
							// recente
							Logger.debug("Leggo il file all'incontrario");
							String[] strings = lines.get(i);
							String rowResult = StringUtils.join(strings, ";");
							// Logger.debug(rowResult);
							if (strings[1].equals("3")) {
								// è uno scanning
								if (strings[2].trim().equals("0")) {
									// iExecResult OK
									String[] res = Arrays.copyOfRange(strings,
											5, strings.length);
									for (int j = 0; j < ids.size(); j++) {
										if (res[j].equals("1")) {
											// elemento visibile
											Device d = this.findDeviceById(ids
													.get(j));
											result.add(d);
										} else {
											Logger.info("elemento non visibile");
										}
									}
								}
								// suppongo di arrestarmi al primo che trovo,
								// essendo partito dal fondo il più recente
								reader.close();
								return result;
							}
						}
						reader.close();

					}
				}
				// conto le volte che passo. Se supero un certo parametro esco
				cycleCount += 1;
				// metto in pasusa per 1 secondo
				try {
					Thread.sleep(FTPConnectionManager.WHILE_SLEEEP);
				} catch (InterruptedException e) {
				}
			}
		} else {
			// il device non è un concentratore devo spare un eccezione
		}
		return result;
	}

	public boolean isDataExist(Device device) throws SocketException,
			IOException {
		if (device.getConcentrator() != null) {
			Logger.debug("Verifico la presenza di dati nel device "
					+ device.toString());
			FTPClient fc = new FTPClient();
			fc.connect(device.getConcentrator().getAddress());
			fc.login(device.getConcentrator().getUsername(), device
					.getConcentrator().getPassword());

			String drFileName = String.format(
					FTPConnectionManager.DATA_READ_TEMPLATE_STRING,
					device.getId());
			InputStream inputStream = fc.retrieveFileStream(drFileName);
			int returnCode = fc.getReplyCode();
			if (inputStream == null || returnCode == 550) {
				return false;
			}
			return true;
		} else {
			// il device non è un concentratore devo spare un eccezione
		}
		return false;
	}

	private String programFileGenerator(List<? extends ProgramIF> elements) {
		String result = "";
		String firstRow = String.format("%s;-1;-1;-1;-1;-1\n", elements.size());
		result += firstRow;
		for (ProgramIF ps : elements) {
			result += ps.toString();
		}
		return result;
	}

	public void saveDataRead(Device device) throws SocketException, IOException {
		if (device.getConcentrator() != null) {
			FTPClient fs = new FTPClient();
			fs.connect(FTPConnectionManager.IP_ADDRESS_FTP_MASTER_SERVER);
			fs.login(FTPConnectionManager.USERNAME_FTP_MASTER_SERVER,
					FTPConnectionManager.PASSWORD_FTP_MASTER_SERVER);
			if (!FTPReply.isPositiveCompletion(fs.getReplyCode())) {
				Logger.debug("Exception in conneting to FTP Server "
						+ FTPConnectionManager.IP_ADDRESS_FTP_MASTER_SERVER);
				fs.disconnect();
			} else {
				Logger.debug("Succesfully connected to "
						+ FTPConnectionManager.IP_ADDRESS_FTP_MASTER_SERVER);
			}
			String drFileName = String.format(
					FTPConnectionManager.DATA_READ_TEMPLATE_STRING,
					device.getId());
			boolean flag = false;
			int cycleCount = 0;
			while (!flag && cycleCount < FTPConnectionManager.MAX_CYCLE_COUNT) {
				// se il file è grosso non è detto che si sia subito sul server,
				// aspetto un po'
				FTPFile[] files = fs.listFiles();
				for (FTPFile ftpFile : files) {
					if (ftpFile.getName().toUpperCase()
							.equals(drFileName.toUpperCase())) {
						Logger.info("trovato data read " + ftpFile.getName()
								+ " nel server lo scarico");
						flag = true;
						// se è presente lo devo svuotare e eliminare
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						fs.retrieveFile(ftpFile.getName(), baos);
						// prima di procedere alla cancellazione dall'area ftp
						// mi
						// salvo il file in una cartella di backup
						String pathname = FTPConnectionManager.PATH_DATA_READ_FILE
								+ FTPConnectionManager.FILE_DATE_FORMATTER
										.format(new Date())
								+ "_"
								+ ftpFile.getName().toUpperCase();
						Logger.debug("Salvo il file su file system");
						FileUtils.writeByteArrayToFile(new File(pathname),
								baos.toByteArray());

						boolean deleted = fs.deleteFile(drFileName);
						if (deleted) {
							Logger.debug("File " + ftpFile.getName()
									+ " deleted");
						}
						BufferedReader bufferedReader = new BufferedReader(
								new InputStreamReader(new ByteArrayInputStream(
										baos.toByteArray())));
						CSVReader reader = new CSVReader(bufferedReader, ';');

						List<String[]> lines = reader.readAll();
						for (String[] strings : lines) {
							// qui devo implementare la scrittura dei risultati
							// nel
							// caso
							// trovi dei dati di ranging o dowloading
							String rowResult = StringUtils.join(strings, ";");
							// Logger.debug(rowResult);
							if (strings[2].trim().equals("0")) {
								// parso il risultato solo se iExecResult è OK
								// cioè uguale a 1
								if (strings[1].equals("3")) {
									// Scanning
									this.saveScanningData(device, strings,
											rowResult);
								} else if (strings[1].equals("2")) {
									// Downloading
									this.saveStaticData(strings, rowResult);
								} else if (strings[1].equals("1")) {
									// Ranging
									this.saveDynamicData(strings, rowResult);
								} else if (strings[1].equals("4")) {
									// Status
									this.saveStatusData(device, strings,
											rowResult);
								} else {
									Logger.debug("Valore non riconosciuto");
								}
							}
						}
						reader.close();
					} else {
						Logger.info("non trovato data read nel server");
					}
				}
				cycleCount += 1;
				// metto in pasusa per 1 secondo
				try {
					Thread.sleep(FTPConnectionManager.WHILE_SLEEEP);
				} catch (InterruptedException e) {
				}
			}
			if (cycleCount >= FTPConnectionManager.MAX_CYCLE_COUNT) {
				Logger.info("Data Read " + device
						+ " non è presente nel server");
			}
			fs.logout();
			fs.disconnect();
		} else {
			// il device non è un concentratore devo spare un eccezione
		}
	}

	private void saveDynamicData(String[] strings, String rowResult) {
		try {
			DynamicData dd = new DynamicData();
			dd.setData(FTPConnectionManager.SQL_DATE_FORMATTER.parse(strings[0]
					.trim()));
			Device nodeDest = this.findDeviceById(Long.parseLong(strings[3]
					.trim()));
			dd.setNodeDest(nodeDest);
			Device nodeRange = this.findDeviceById(Long.parseLong(strings[4]
					.trim()));
			dd.setNodeRange(nodeRange);
			Integer numSamples = Integer.valueOf(strings[5].trim());
			dd.setNumSamples(numSamples);
			for (int j = 6; j < 6 + numSamples; j++) {
				Double d = Double.parseDouble(strings[j].trim().replace(",",
						"."));
				DynamicValues dv = new DynamicValues();
				dv.setRanging(new BigDecimal(d));
				dv.setDynamicData(dd);
				dd.getValues().add(dv);
			}

			dd.setRawData(rowResult);
			this.saveObject(dd);
		} catch (ParseException e) {
			Logger.debug("Errore nel parsong dei dati valori numerici di ranging");
			Logger.debug(rowResult);
			e.printStackTrace();
		} catch (Exception e) {
			Logger.debug("Errore nella lettura dei dati di ranging");
			Logger.debug(rowResult);
			e.printStackTrace();
		}
	}

	private void saveObject(final Object o) {
		Callback0 callback0 = new Callback0() {
			@SuppressWarnings("unchecked")
			public void esegui() {
				JPA.em().persist(o);
				JPA.em().flush();
			}

			@Override
			public void invoke() throws Throwable {
				this.esegui();
			}
		};
		try {
			JPA.em();
			try {
				callback0.invoke();
			} catch (Throwable e) {
				Logger.error("Errore nel salvataggio dell'oggetto "
						+ o.toString());
			}
		} catch (Exception e) {
			try {
				JPA.withTransaction(callback0);
			} catch (Exception e1) {
				Logger.error("Errore nel salvataggio dell'oggetto "
						+ o.toString());
			}
		}
	}

	private void saveScanningData(Device device, String[] strings,
			String rowResult) {
		try {
			ScanningData sd = new ScanningData();
			sd.setData(FTPConnectionManager.SQL_DATE_FORMATTER.parse(strings[0]
					.trim()));
			sd.setNodeDest(device);
			sd.setNodeStartId(Long.parseLong(strings[3].trim()));
			sd.setNodeEndId(Long.parseLong(strings[4].trim()));
			String[] res = Arrays.copyOfRange(strings, 5, strings.length);
			String result = "";
			for (int j = 0; j < res.length; j++) {
				result += res[j] + ";";
			}
			sd.setResult(result);
			sd.setRawData(rowResult);
			this.saveObject(sd);
		} catch (Exception e) {
			Logger.debug("Errore nella lettura dei dati di scanning");
			Logger.debug(rowResult);
			e.printStackTrace();
		}
	}

	private void saveStaticData(String[] strings, String rowResult) {
		try {
			StaticData sd = new StaticData();
			sd.setData(FTPConnectionManager.SQL_DATE_FORMATTER.parse(strings[0]
					.trim()));
			Device nodeDest = this.findDeviceById(Long.parseLong(strings[3]
					.trim()));
			sd.setNodeDest(nodeDest);
			if (strings[4].trim().equals("1")) {
				sd.setDataType(DataType.INCLINAZIONE);
			} else if (strings[4].trim().equals("2")) {
				sd.setDataType(DataType.ACCELEROMETRO);
			} else if (strings[4].trim().equals("3")) {
				sd.setDataType(DataType.GIROSCOPIO);
			} else if (strings[4].trim().equals("4")) {
				sd.setDataType(DataType.CAMPOMAGNETICO);
			} else {
				sd.setDataType(null);
			}
			Integer numTriples = Integer.valueOf(strings[5].trim());
			sd.setNumTriples(numTriples);
			for (int j = 6; j < 6 + 3 * numTriples; j = j + 3) {
				Double x = Double.parseDouble(strings[j].trim().replace(",",
						"."));
				Double y = Double.parseDouble(strings[j + 1].trim().replace(
						",", "."));
				Double z = Double.parseDouble(strings[j + 2].trim().replace(
						",", "."));
				StaticValues sv = new StaticValues();
				sv.setValueX(new BigDecimal(x));
				sv.setValueY(new BigDecimal(y));
				sv.setValueZ(new BigDecimal(z));
				sv.setStaticData(sd);
				sd.getValues().add(sv);
			}
			sd.setRawData(rowResult);
			this.saveObject(sd);
		} catch (ParseException e) {
			Logger.debug("Errore nel parsong dei dati valori numerici di downloading");
			Logger.debug(rowResult);
			e.printStackTrace();
		} catch (Exception e) {
			Logger.debug("Errore nella lettura dei dati di downloading");
			Logger.debug(rowResult);
			e.printStackTrace();
		}
	}

	private void saveStatusData(Device device, String[] strings,
			String rowResult) {
		try {
			StatusData sd = new StatusData();
			sd.setData(FTPConnectionManager.SQL_DATE_FORMATTER.parse(strings[0]
					.trim()));
			sd.setNodeDest(device);
			Integer numValues = Integer.valueOf(strings[3].trim());
			if (numValues <= 4) {
				// sono nella versione del protocollo con al massimo 4 valori
				// definiti
				Double v = Double.parseDouble(strings[4].trim().replace(",",
						"."));
				sd.setBatteryVoltage(new BigDecimal(v).setScale(1,
						RoundingMode.HALF_DOWN));
				if (strings[5].trim().equals("0")) {
					sd.setGpsError(false);
				} else {
					sd.setGpsError(true);
				}
				if (strings[6].trim().equals("0")) {
					sd.setFtpError(false);
				} else {
					sd.setFtpError(true);
				}
				if (strings[7].trim().equals("0")) {
					sd.setSchedulerError(false);
				} else {
					sd.setSchedulerError(true);
				}
			}
			sd.setRawData(rowResult);
			this.saveObject(sd);
		} catch (ParseException e) {
			Logger.debug("Errore nel parsong dei dati valori numerici di status");
			Logger.debug(rowResult);
			e.printStackTrace();
		} catch (Exception e) {
			Logger.debug("Errore nella lettura dei dati di status");
			Logger.debug(rowResult);
			e.printStackTrace();
		}
	}

	public void sendGetFiles(Device device) throws SocketException, IOException {
		if (device.getConcentrator() != null) {
			// l'oggetto è un concentratore posso inviarli una programmazione
			// devo collegarmi al server ftp del PLC
			FTPClient fc = new FTPClient();
			fc.connect(device.getConcentrator().getAddress());
			fc.login(device.getConcentrator().getUsername(), device
					.getConcentrator().getPassword());

			String gfFileName = String.format(
					FTPConnectionManager.GET_FILE_TEMPLATE_STRING,
					device.getId());
			byte[] emptyArray = new byte[0];
			ByteArrayInputStream byteaos = new ByteArrayInputStream(emptyArray);
			Logger.debug("carico get files nel concentratore");
			fc.storeFile(gfFileName, byteaos);
			boolean flag = true;
			int cycleCount = 0;
			while (flag && cycleCount < FTPConnectionManager.MAX_CYCLE_COUNT) {
				// ciclo finchè non lo trovo più
				// dovrei metterci un controllo sul tempo al while precedente
				// altrimenti rischio di rimanere piantato
				FTPFile[] files = fc.listFiles();
				flag = false;
				for (FTPFile ftpFile : files) {
					if (ftpFile.getName().toUpperCase()
							.equals(gfFileName.toUpperCase())) {
						Logger.debug("File " + ftpFile.getName()
								+ " ancora presente sul device");
						flag = true;
						break;
					}
				}
				cycleCount += 1;
				// metto in pasusa per 1 secondo
				try {
					Thread.sleep(FTPConnectionManager.WHILE_SLEEEP);
				} catch (InterruptedException e) {
				}

			}
			fc.logout();
			fc.disconnect();

		} else {
			// il device non è un concentratore devo spare un eccezione
		}

	}

	public void sendProgramFile(Device device,
			List<? extends ProgramIF> elements) throws SocketException,
			IOException {
		if (device.getConcentrator() != null) {
			// l'oggetto è un concentratore posso invierli una programmazione
			String pwFileName = String.format(
					FTPConnectionManager.PROGRAM_WRITE_TEMPLATE_STRING,
					device.getId());
			byte[] ba = this.programFileGenerator(elements).getBytes();
			ByteArrayInputStream bais = new ByteArrayInputStream(ba);

			// devo collegarmi al server ftp del PLC
			FTPClient fc = new FTPClient();

			fc.connect(device.getConcentrator().getAddress());
			fc.login(device.getConcentrator().getUsername(), device
					.getConcentrator().getPassword());

			if (!FTPReply.isPositiveCompletion(fc.getReplyCode())) {
				Logger.debug("Exception in conneting to FTP Server "
						+ device.getConcentrator().getAddress());
				fc.disconnect();
			} else {
				Logger.debug("Succesfully connected to "
						+ device.getConcentrator().getAddress());
			}
			// cosi carico la programmazione dello scanning
			fc.storeFile(pwFileName, bais);
			Logger.debug(pwFileName + " caricato sul device "
					+ device.toString());
			// a questo punto dovrei verificare che questo file venga cancellato
			// dall'area ftp
			boolean flag = true;
			int cycleCount = 0;
			while (flag && cycleCount < FTPConnectionManager.MAX_CYCLE_COUNT) {
				// ciclo finchè non lo trovo più
				// dovrei metterci un controllo sul tempo al while precedente
				// altrimenti rischio di rimanere piantato
				FTPFile[] files = fc.listFiles();
				flag = false;
				for (FTPFile ftpFile : files) {
					if (ftpFile.getName().toUpperCase()
							.equals(pwFileName.toUpperCase())) {
						Logger.debug("File di programmazione "
								+ ftpFile.getName() + " ancora presente");
						flag = true;
						break;
					}
				}
				cycleCount += 1;
				// metto in pasusa per 1 secondo
				try {
					Thread.sleep(FTPConnectionManager.WHILE_SLEEEP);
				} catch (InterruptedException e) {
				}
			}
			fc.logout();
			fc.disconnect();
		} else {
			// il device non è un concentratore devo spare un eccezione
		}
	}
}
