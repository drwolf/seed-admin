package ftp.model;

public class ProgramScanning implements ProgramIF {

	private final static Integer CMD_CODE = 3;

	private Integer iTime;

	private Integer iInterval;

	private Long iNodeDest;

	private Long iStartAddress;

	private Long iEndAddress;

	public ProgramScanning() {
		super();
	}

	public ProgramScanning(Integer iTime, Integer iInterval, Long iNodeDest,
			Long iStartAddress, Long iEndAddress) {
		super();
		this.iTime = iTime;
		this.iInterval = iInterval;
		this.iNodeDest = iNodeDest;
		this.iStartAddress = iStartAddress;
		this.iEndAddress = iEndAddress;
	}

	@Override
	public Integer getCmdCode() {
		return ProgramScanning.CMD_CODE;
	}

	public Long getiEndAddress() {
		return this.iEndAddress;
	}

	@Override
	public Integer getiInterval() {
		return this.iInterval;
	}

	@Override
	public Long getiNodeDest() {
		return this.iNodeDest;
	}

	public Long getiStartAddress() {
		return this.iStartAddress;
	}

	@Override
	public Integer getiTime() {
		return this.iTime;
	}

	public void setiEndAddress(Long iEndAddress) {
		this.iEndAddress = iEndAddress;
	}

	@Override
	public void setiInterval(Integer iInterval) {
		this.iInterval = iInterval;
	}

	@Override
	public void setiNodeDest(Long iNodeDest) {
		this.iNodeDest = iNodeDest;
	}

	public void setiStartAddress(Long iStartAddress) {
		this.iStartAddress = iStartAddress;
	}

	@Override
	public void setiTime(Integer iTime) {
		this.iTime = iTime;
	}

	@Override
	public String toString() {
		String placeholder = "%d;%d;%d;%d;%d;%d";
		return String.format(placeholder, ProgramScanning.CMD_CODE,
				this.getiTime(), this.getiInterval(), this.getiNodeDest(),
				this.getiStartAddress(), this.getiEndAddress());
	}
}
