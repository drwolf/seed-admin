package ftp.model;

public interface ProgramIF {

	public Integer getCmdCode();

	public Integer getiInterval();

	public Long getiNodeDest();

	public Integer getiTime();

	public void setiInterval(Integer iInterval);

	public void setiNodeDest(Long iNodeDest);

	public void setiTime(Integer iTime);
}
