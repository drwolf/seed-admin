package keys;

public class SessionKeys {

	public static final String USERNAME = "username";
	public static final String ADMIN = "admin";
	public static final String USER_LEVEL = "userlevel";
}
