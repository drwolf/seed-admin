package keys;

public class FlashKeys {

	public static final String SUCCESS = "success";
	public static final String WARNING = "warning";
	public static final String INFO = "info";
	public static final String ERROR = "error";

}
