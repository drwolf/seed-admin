package controllers;

import java.util.List;

import keys.FlashKeys;
import keys.SessionKeys;
import model.User;
import play.Logger;
import play.Play;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.BodyParser.Of;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.Security.Authenticated;
import controllers.security.Secured;

public class LoginController extends Controller {

	public static class Login {

		private String username;

		private String password;

		private String from;

		public String getFrom() {
			return this.from;
		}

		public String getPassword() {
			return this.password;
		}

		public String getUsername() {
			return this.username;
		}

		public void setFrom(String from) {
			this.from = from;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public void setUsername(String username) {
			this.username = username;
		}

	}

	@Of(BodyParser.Json.class)
	@SuppressWarnings("unchecked")
	@Transactional
	public static Result authenticate() {

		Form<Login> loginForm = Form.form(Login.class).bindFromRequest();
		if (loginForm.hasErrors()) {
			return Results.badRequest("Errore di autenticazione");
		}

		String username = loginForm.get().username;
		Logger.debug("username: " + username);
		Logger.debug("password: " + loginForm.get().password);
		List<User> userList = JPA.em()
				.createQuery("from User where email=:email")
				.setParameter("email", username).getResultList();
		if (!userList.isEmpty()) {
			if (userList.size() > 1) {
				String errorMsg = "Multiple users with the same email! Contact the administrator!";
				Logger.error(String.format("Multiple users with email %s !",
						username));
				return Results.badRequest(errorMsg);
			}
			User user = userList.get(0);
			if (!user.getActive()) {
				String errorMsg = String.format("User '%s' is not active",
						username);
				return Results.badRequest(errorMsg);
			}
			if (user.getPassword().equals(loginForm.get().password)) {

				if (user.isAdmin()) {
					Controller.session(SessionKeys.ADMIN, "true");
				}
				Controller.session(SessionKeys.USERNAME, username);

				// TODO: gestire ruoli
				// Controller.session(SessionKeys.USER_LEVEL, user.getLevel()
				// .name());
				Logger.info(String.format("user %s logged in", user));
				String fromUrl = loginForm.get().getFrom();
				String appContextUrl = Play.application().configuration()
						.getString("application.context");
				// if (fromUrl != null && !fromUrl.equals("")
				// && fromUrl.startsWith(appContextUrl)) {
				// Logger.debug("from: " + fromUrl);
				// return Results.redirect(fromUrl);
				// }
				return Results.ok(Json.toJson("Benvenuto " + username));
			}

		}
		Controller.flash(FlashKeys.ERROR, "Access Denied!");
		return Results.badRequest("I dati inseriti non sono corretti");

	}

	@SuppressWarnings("unchecked")
	@Of(BodyParser.Json.class)
	@Transactional
	@Authenticated(Secured.class)
	public static Result getUser() {
		User user = null;
		String username = session(SessionKeys.USERNAME);
		List<User> userList = JPA.em()
				.createQuery("from User where email=:email")
				.setParameter("email", username).getResultList();
		if (!userList.isEmpty()) {
			user = userList.get(0);
		}
		return Results.ok(Json.toJson(user));
	}

	@Of(BodyParser.Json.class)
	public static Result logout() {
		Controller.session().clear();
		return Results.ok(Json.toJson("logged out"));
	}

}
