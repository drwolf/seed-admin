package controllers;

import java.util.ArrayList;
import java.util.List;

import model.Device;
import model.Device.DeviceType;
import model.DynamicValues;
import model.Site;
import model.Site.SiteStatus;
import model.StaticValues;
import model.Views.SiteDriven;
import model.Views.ValuesDriven;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.BodyParser.Of;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.Security.Authenticated;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import controllers.security.Secured;
import ftp.SiteProgrammer;

@Authenticated(Secured.class)
@SuppressWarnings("unchecked")
public class SiteController extends Controller {

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result children(Long id) {
		try {
			List<Device> children = new ArrayList<Device>();

			Device device = JPA.em().find(Device.class, id);
			// per ora presumo che l'elemnto a cui si chiede lo scanning sia
			// collegato ad un concentratore.
			if (device.getConcentrator() != null) {
				List<Device> results = new ArrayList<Device>();
				SiteProgrammer siteProgrammer = new SiteProgrammer();

				if (device.getSite().getStatus()
						.equals(SiteStatus.CONFIGURATION)) {
					// se il sito non è in registrazione procedo con una
					// richiesta diretta al sito
					Logger.debug("Eseguo lo scan del device "
							+ device.toString() + " da FTP");
					results = siteProgrammer.scanningFromFTP(device);
				} else {
					// se il sito è in registrazione mostro il risultato
					// dell'ultimo scanning memorizzato sul db
					Logger.debug("Eseguo lo scan del device "
							+ device.toString() + " da DB");
					results = siteProgrammer.scanningFromDB(device);
				}

				for (Device d : results) {
					if (d.getLat() != null && d.getLon() != null
							&& d.getType() == DeviceType.SLAVE
							&& !d.getId().equals(id)) {
						children.add(d);
					}
				}

			}

			String childresAsJson = SiteController.getSiteDrivenWriter()
					.writeValueAsString(children);
			Logger.debug("---> CHILDREN: " + childresAsJson);
			return Results.ok(childresAsJson);
		} catch (Exception e) {
			Logger.error("Errore nel recupero devices figli " + id, e);
			return Results
					.internalServerError(Json
							.toJson("Errore nel recupero devices figli per concentratore con id:"
									+ id));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result delete(Long id) {
		try {
			Site s = JPA.em().find(Site.class, id);
			JPA.em().remove(s);
			return Results.ok(SiteController.getSiteDrivenWriter()
					.writeValueAsString(s));
		} catch (Exception e) {
			Logger.error("Errore nella cancellazione del sito " + id, e);
			return Results.internalServerError(Json
					.toJson("Errore nella cancellazione del sito"));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result getAll() {

		try {
			List<Site> resultList = JPA.em().createQuery("from Site")
					.getResultList();
			String listAsJson = SiteController.getSiteDrivenWriter()
					.writeValueAsString(resultList);
			Logger.debug(listAsJson);
			return Results.ok(listAsJson);
		} catch (Exception e) {
			Logger.error("Errore in recupero lista siti", e);
			return Results.internalServerError(Json
					.toJson("Errore in recupero lista siti"));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result getById(Long id) {
		String jsonSite;
		try {
			jsonSite = SiteController.getSiteDrivenWriter().writeValueAsString(
					JPA.em().find(Site.class, id));
			Logger.debug("LOAD --->" + jsonSite.toString());
			return Results.ok(jsonSite);
		} catch (Exception e) {
			Logger.error("Errore nel recupero dati del sito " + id, e);
			return Results.internalServerError(Json
					.toJson("Errore nel recupero dati del sito"));
		}

	}

	public static ObjectWriter getDataDrivenWriter() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
		return mapper.writerWithView(ValuesDriven.class);
	}

	// STUB
	@Transactional
	@Of(BodyParser.Json.class)
	public static Result getDynamicData(Long id, Long targetId) {
		try {
			List<DynamicValues> resultList = JPA
					.em()
					.createQuery(
							"from DynamicValues dv where dv.dynamicData.nodeDest.id=:id and dv.dynamicData.nodeRange.id=:tid order by dv.dynamicData.data desc")
					.setParameter("id", id).setParameter("tid", targetId)
					.setMaxResults(500).getResultList();
			String dataAsJson = SiteController.getDataDrivenWriter()
					.writeValueAsString(resultList);
			Logger.info(dataAsJson);
			return Results.ok(dataAsJson);
		} catch (Exception e) {
			Logger.error(
					String.format(
							"Errore nel recupero dynamic data tra concentrator_%s e slave_%s",
							id, targetId), e);
			return Results
					.internalServerError(Json.toJson(String
							.format("Errore nel recupero dynamic data tra concentrator_%s e slave_%s",
									id, targetId)));
		}
	}

	public static ObjectWriter getSiteDrivenWriter() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
		return mapper.writerWithView(SiteDriven.class);
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result getStaticData(Long id) {
		try {
			List<StaticValues> resultList = JPA
					.em()
					.createQuery(
							"from StaticValues sv where sv.staticData.nodeDest.id=:id order by sv.staticData.data desc")
					.setParameter("id", id).setMaxResults(10).getResultList();
			String dataAsJson = SiteController.getDataDrivenWriter()
					.writeValueAsString(resultList);
			Logger.info(dataAsJson);
			return Results.ok(dataAsJson);
		} catch (Exception e) {
			Logger.error("Errore nel recupero static data per device con id:"
					+ id, e);
			return Results
					.internalServerError(Json
							.toJson("Errore nel recupero static data per device con id:"
									+ id));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result save() {
		try {
			JsonNode site = Controller.request().body().asJson();
			Site s = Json.fromJson(site, Site.class);
			JPA.em().persist(s);
			return Results.ok(SiteController.getSiteDrivenWriter()
					.writeValueAsString(s));
		} catch (Exception e) {
			Logger.error("Errore nel salvataggio del sito ", e);
			return Results.internalServerError(Json
					.toJson("Errore nel salvataggio del sito"));
		}
	}

	// // STUB
	@Transactional
	@Of(BodyParser.Json.class)
	public static Result start(Long id) {
		try {
			JsonNode data = Controller.request().body().asJson();
			String speed = data.get("speed").asText();

			Thread.sleep(1000);
			Site site = JPA.em().find(Site.class, id);
			site.setStatus(SiteStatus.RECORDING);
			JPA.em().persist(site);
			Logger.debug(String.format("---> START %s: %s", speed, id));
			return Results.ok(Json.toJson(SiteStatus.RECORDING));
		} catch (Exception e) {
			Logger.error("Errore inizio elabrazioni dati sito " + id, e);
			return Results.internalServerError(Json
					.toJson("Errore inizio elabrazioni"));
		}
	}

	// //STUB
	@Transactional
	@Of(BodyParser.Json.class)
	public static Result stop(Long id) {
		try {
			Thread.sleep(1000);
			Site site = JPA.em().find(Site.class, id);
			site.setStatus(SiteStatus.STOP);
			JPA.em().persist(site);
			Logger.debug("---> STOP: " + id);
			return Results.ok(Json.toJson(SiteStatus.STOP));
		} catch (Exception e) {
			Logger.error("Errore fine elabrazione dati sito " + id, e);
			return Results.internalServerError(Json
					.toJson("Errore fine elabrazione"));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result update(Long id) {
		try {
			JsonNode site = Controller.request().body().asJson();
			Logger.debug("UPDATE --->" + site.toString());
			Site s = Json.fromJson(site, Site.class);
			for (Device d : s.getDevices()) {
				d.setSite(s);
			}
			JPA.em().merge(s);
			return Results.ok(SiteController.getSiteDrivenWriter()
					.writeValueAsString(s));
		} catch (Exception e) {
			Logger.error("Errore nell'aggiornamento del sito " + id, e);
			return Results.internalServerError(Json
					.toJson("Errore nell'aggiornamento del sito"));
		}
	}

}
