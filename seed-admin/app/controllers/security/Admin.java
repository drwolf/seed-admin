package controllers.security;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import keys.SessionKeys;
import play.Logger;
import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.With;
import controllers.security.Admin.AdminAction;

@With(AdminAction.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
@Inherited
@Documented
public @interface Admin {

	public class AdminAction extends Action<Result> {

		@Override
		public Promise<Result> call(Context context) throws Throwable {
			Status unauthorized = Results
					.unauthorized("Per eseguire questa azione sono necessari i privilegi di \"AMMINISTRATORE\"");

			String username = Controller.session(SessionKeys.USERNAME);
			if (username != null && !username.equals("")) {
				String admin = Controller.session(SessionKeys.ADMIN);
				if (admin != null && admin.equals("true")) {
					return this.delegate.call(context);
				} else {
					Logger.warn(String.format(
							"User \"%s\" tried to execute admin action",
							username));
				}
			}

			return Promise.pure((Result) unauthorized);
		}

	}

}
