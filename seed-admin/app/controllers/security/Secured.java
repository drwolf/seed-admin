package controllers.security;

import keys.SessionKeys;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.Security.Authenticator;

public class Secured extends Authenticator {

	public static Boolean isAdmin() {
		return Context.current().session().get(SessionKeys.ADMIN) != null;
	}

	public static Boolean isLogged() {
		return Context.current().session().get(SessionKeys.USERNAME) != null;
	}

	@Override
	public String getUsername(Context ctx) {
		return ctx.session().get(SessionKeys.USERNAME);
	}

	@Override
	public Result onUnauthorized(Context ctx) {
		String path = Context.current().request().path();
		// return Results.redirect(routes.LoginController.index(path));
		return Results.unauthorized();
	}

}
