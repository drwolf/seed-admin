package controllers;

import java.util.List;

import model.User;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import play.libs.F.Callback0;
import schedulers.BatteryStatusScheduler;
import schedulers.FTPScheduler;

public class Global extends GlobalSettings {

	@Override
	public void onStart(Application application) {

		Logger.info(String.format("dev: %s, prod: %s, test: %s", Play.isDev(),
				Play.isProd(), Play.isTest()));
		JPA.withTransaction(new Callback0() {

			@SuppressWarnings("unchecked")
			private void checkAndCreateUser(String email, String pwd,
					boolean active, boolean admin) {
				List<User> userList = JPA.em()
						.createQuery("from User where email=:email")
						.setParameter("email", email).getResultList();
				if (userList.isEmpty()) {
					User user = new User();
					user.setActive(active);
					user.setEmail(email);
					user.setPassword(pwd);
					user.setAdmin(admin);
					JPA.em().persist(user);
				}
			}

			@Override
			public void invoke() throws Throwable {
				this.checkAndCreateUser("admin@drwolf.it", "admin", true, true);
				this.checkAndCreateUser("user@drwolf.it", "user", true, false);
			}
		});

		super.onStart(application);

		FTPScheduler.scheduler();
		BatteryStatusScheduler.dailyScheduler();
		BatteryStatusScheduler.hourlyScheduler();

	}

	@Override
	public void onStop(Application application) {
		Logger.info("Application shutdown...");
		super.onStop(application);
	}

}
