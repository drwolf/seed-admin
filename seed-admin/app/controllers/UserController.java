package controllers;

import java.util.List;

import model.User;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.BodyParser.Of;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;

import com.fasterxml.jackson.databind.JsonNode;

import controllers.security.Admin;
import controllers.security.Secured;

@Admin
@Authenticated(Secured.class)
public class UserController extends Controller {

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result delete(Long id) {
		try {
			User d = JPA.em().find(User.class, id);
			JPA.em().remove(d);
			return ok(Json.toJson(d));
		} catch (Exception e) {
			Logger.error("Errore nella cancellazione dello user " + id, e);
			return internalServerError(Json
					.toJson("Errore nella cancellazione dello user"));
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Of(BodyParser.Json.class)
	public static Result getAll() {
		try {
			List<User> resultList = JPA.em().createQuery("from User")
					.getResultList();

			Logger.debug("---> LOAD: " + Json.toJson(resultList));
			return ok(Json.toJson(resultList));

		} catch (Exception e) {
			Logger.error("Errore nel recupero dello user", e);
			return internalServerError(Json.toJson("Errore nel recupero user"));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result getById(Long id) {
		try {
			return ok(Json.toJson(JPA.em().find(User.class, id)));
		} catch (Exception e) {
			Logger.error("Errore nel recupero dell'utente " + id, e);
			return internalServerError(Json
					.toJson("Errore nel recupero dell'utente"));
		}
	}

	@SuppressWarnings("rawtypes")
	protected static Result save(Long id) {
		try {
			JsonNode device = request().body().asJson();
			Logger.debug("---> SAVE: " + device.toString());
			User d = Json.fromJson(device, User.class);

			List resultList = JPA.em()
					.createQuery("from User where email =:email and id!=:id")
					.setParameter("email", d.getEmail())
					.setParameter("id", id != null ? id : 0).getResultList();

			String matNotUniqueMsg = "L'indirizzo email '" + d.getEmail()
					+ "' è già stata utilizzato";

			if (id != null) {
				if (!id.equals(d.getId())) {
					Logger.error(String
							.format("L'id >%s< passato nella chiamata è diverso da quello serializzato in json >%s<",
									id, d.getId()));
					return internalServerError(Json
							.toJson("Si è verificato un problema di incoerenza sui dati"));
				}

				if (!resultList.isEmpty()) {
					return badRequest(Json.toJson(matNotUniqueMsg));
				}
				JPA.em().merge(d);

			} else {
				if (!resultList.isEmpty()) {
					return badRequest(Json.toJson(matNotUniqueMsg));
				}
				JPA.em().persist(d);
			}

			JPA.em().flush();
			return ok(Json.toJson(d).toString());

		} catch (Throwable e) {
			Logger.error("Errore in aggiornamento device " + id, e);
			return internalServerError(Json.toJson(e.getMessage()));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result store() {
		return save(null);
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result update(Long id) {
		return save(id);
	}

}
