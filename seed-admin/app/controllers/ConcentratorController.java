package controllers;

import java.util.List;

import javax.persistence.Query;

import model.Concentrator;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.BodyParser.Of;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectWriter;

import controllers.security.Secured;

@Authenticated(Secured.class)
public class ConcentratorController extends Controller {

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result delete(Long id) {
		try {
			Concentrator d = JPA.em().find(Concentrator.class, id);
			JPA.em().remove(d);
			return ok(DeviceController.getDeviceDrivenWriter()
					.writeValueAsString(d));
		} catch (Exception e) {
			Logger.error("Errore nella cancellazione del device " + id, e);
			return internalServerError(Json
					.toJson("Errore nella cancellazione del device"));
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Of(BodyParser.Json.class)
	public static Result getAll(scala.Option<Long> deviceId,
			scala.Option<Boolean> available) {
		try {
			String queryTxt = "from Concentrator c";
			if (available.isDefined() && !available.isEmpty()) {
				String subQuery = "select d.concentrator from Device d where d.concentrator!=null";
				if (deviceId.isDefined() && !deviceId.isEmpty()) {
					subQuery = subQuery.concat(" and d.id!=:deviceId");
				}
				queryTxt = queryTxt
						.concat(" where c not in (" + subQuery + ")");
			}

			queryTxt = queryTxt.concat(" order by id");
			Query query = JPA.em().createQuery(queryTxt);
			if (available.isDefined() && !available.isEmpty()
					&& deviceId.isDefined() && !deviceId.isEmpty()) {
				query.setParameter("deviceId", deviceId.get());
			}
			List<Concentrator> resultList = query.getResultList();

			JsonNode listAsJson = Json.toJson(resultList);
			Logger.debug("---> LOAD: " + listAsJson);
			return ok(listAsJson);

		} catch (Exception e) {
			Logger.error("Errore nel recupero concentratori", e);
			return internalServerError(Json
					.toJson("Errore nel recupero concentratori"));
		}
	}

	@SuppressWarnings("unchecked")
	protected static Result save(Long id, ObjectWriter writer) {
		try {
			JsonNode device = request().body().asJson();
			Logger.debug("---> SAVE: " + device.toString());
			Concentrator d = Json.fromJson(device, Concentrator.class);

			List<Concentrator> resultList = JPA
					.em()
					.createQuery(
							"from Concentrator where matricola =:matricola and id!=:id")
					.setParameter("matricola", d.getMatricola())
					.setParameter("id", id != null ? id : 0).getResultList();

			String matNotUniqueMsg = "La matricola '" + d.getMatricola()
					+ "' è già stata utilizzata";

			if (id != null) {
				if (!id.equals(d.getId())) {
					Logger.error(String
							.format("L'id >%s< passato nella chiamata è diverso da quello serializzato in json >%s<",
									id, d.getId()));
					return internalServerError(Json
							.toJson("Si è verificato un problema di incoerenza sui dati"));
				}

				if (!resultList.isEmpty()) {
					return badRequest(Json.toJson(matNotUniqueMsg));
				}
				JPA.em().merge(d);

			} else {
				if (!resultList.isEmpty()) {
					return badRequest(Json.toJson(matNotUniqueMsg));
				}
				JPA.em().persist(d);
			}

			JPA.em().flush();
			return ok(writer != null ? writer.writeValueAsString(d) : Json
					.toJson(d).toString());

		} catch (Throwable e) {
			Logger.error("Errore in aggiornamento device " + id, e);
			return internalServerError(Json.toJson(e.getMessage()));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result store() {
		return save(null, null);
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result update(Long id) {
		return save(id, null);
	}

}
