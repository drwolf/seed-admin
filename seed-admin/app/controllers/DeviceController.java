package controllers;

import java.util.List;

import javax.persistence.Query;

import model.Device;
import model.Device.DeviceType;
import model.Views.DeviceDriven;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.BodyParser.Of;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import controllers.security.Secured;

@Authenticated(Secured.class)
@SuppressWarnings("unchecked")
public class DeviceController extends Controller {

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result delete(Long id) {
		try {
			Device d = JPA.em().find(Device.class, id);
			JPA.em().remove(d);
			return ok(getDeviceDrivenWriter().writeValueAsString(d));
		} catch (Exception e) {
			Logger.error("Errore nella cancellazione del device " + id, e);
			return internalServerError(Json
					.toJson("Errore nella cancellazione del device"));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result getAll(scala.Option<Long> siteId,
			scala.Option<Boolean> available) {
		try {
			String queryTxt = "from Device";
			if (available.isDefined() && !available.isEmpty()) {
				queryTxt = queryTxt.concat(" where site=null");
				if (siteId.isDefined() && !siteId.isEmpty()) {
					queryTxt = queryTxt.concat(" or site.id=:siteId");
				}
			}

			queryTxt = queryTxt.concat(" order by id");
			Query query = JPA.em().createQuery(queryTxt);
			if (available.isDefined() && !available.isEmpty()
					&& siteId.isDefined() && !siteId.isEmpty()) {
				query.setParameter("siteId", siteId.get());
			}
			List<Device> resultList = query.getResultList();

			String listAsJson = getDeviceDrivenWriter().writeValueAsString(
					resultList);
			Logger.debug("---> LOAD: " + listAsJson);
			return ok(listAsJson);

		} catch (Exception e) {
			Logger.error("Errore nel recupero devices", e);
			return internalServerError(Json
					.toJson("Errore nel recupero devices"));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result getById(Long id) {
		try {
			return ok(getDeviceDrivenWriter().writeValueAsString(
					JPA.em().find(Device.class, id)));
		} catch (JsonProcessingException e) {
			Logger.error("Errore nel recupero del sito " + id, e);
			return internalServerError(Json
					.toJson("Errore nel recupero del sito"));
		}
	}

	public static ObjectWriter getDeviceDrivenWriter() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
		return mapper.writerWithView(DeviceDriven.class);
	}

	@Of(BodyParser.Json.class)
	public static Result getDeviceTypes() {
		DeviceType[] values = DeviceType.values();
		JsonNode json = Json.toJson(values);
		return ok(json);
	}

	@SuppressWarnings("rawtypes")
	protected static Result save(Long id, ObjectWriter writer) {
		try {
			JsonNode device = request().body().asJson();
			Logger.debug("---> SAVE: " + device.toString());
			Device d = Json.fromJson(device, Device.class);

			List resultList = JPA
					.em()
					.createQuery(
							"from Device where matricola =:matricola and id!=:id")
					.setParameter("matricola", d.getMatricola())
					.setParameter("id", id != null ? id : 0).getResultList();

			String matNotUniqueMsg = "La matricola '" + d.getMatricola()
					+ "' è già stata utilizzata";

			if (id != null) {
				if (!id.equals(d.getId())) {
					Logger.error(String
							.format("L'id >%s< passato nella chiamata è diverso da quello serializzato in json >%s<",
									id, d.getId()));
					return internalServerError(Json
							.toJson("Si è verificato un problema di incoerenza sui dati"));
				}

				if (!resultList.isEmpty()) {
					return badRequest(Json.toJson(matNotUniqueMsg));
				}
				JPA.em().merge(d);

			} else {
				if (!resultList.isEmpty()) {
					return badRequest(Json.toJson(matNotUniqueMsg));
				}
				JPA.em().persist(d);
			}

			JPA.em().flush();
			return ok(writer != null ? writer.writeValueAsString(d) : Json
					.toJson(d).toString());

		} catch (Throwable e) {
			Logger.error("Errore in aggiornamento device " + id, e);
			return internalServerError(Json.toJson(e.getMessage()));
		}
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result store() {
		return save(null, getDeviceDrivenWriter());
	}

	@Transactional
	@Of(BodyParser.Json.class)
	public static Result update(Long id) {
		return save(id, getDeviceDrivenWriter());
	}

}
