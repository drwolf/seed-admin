package model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import model.Views.ValuesDriven;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class StatusData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String rawData;

	@JsonView({ ValuesDriven.class })
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	@ManyToOne
	private Device nodeDest;

	@Column(scale = 1, precision = 3)
	private BigDecimal batteryVoltage;

	@Column
	private boolean gpsError;

	@Column
	private boolean ftpError;

	@Column
	private boolean schedulerError;

	public BigDecimal getBatteryVoltage() {
		return this.batteryVoltage;
	}

	public Date getData() {
		return this.data;
	}

	public Long getId() {
		return this.id;
	}

	public Device getNodeDest() {
		return this.nodeDest;
	}

	public String getRawData() {
		return this.rawData;
	}

	public boolean isFtpError() {
		return this.ftpError;
	}

	public boolean isGpsError() {
		return this.gpsError;
	}

	public boolean isSchedulerError() {
		return this.schedulerError;
	}

	public void setBatteryVoltage(BigDecimal batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setFtpError(boolean ftpError) {
		this.ftpError = ftpError;
	}

	public void setGpsError(boolean gpsError) {
		this.gpsError = gpsError;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNodeDest(Device nodeDest) {
		this.nodeDest = nodeDest;
	}

	public void setRawData(String rawData) {
		this.rawData = rawData;
	}

	public void setSchedulerError(boolean schedulerError) {
		this.schedulerError = schedulerError;
	}

	@Override
	public String toString() {
		return this.getRawData();
	}

}
