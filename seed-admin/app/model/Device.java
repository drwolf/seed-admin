package model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import model.Views.DeviceDriven;
import model.Views.SiteDriven;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import play.Logger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Audited
@JsonIgnoreProperties(ignoreUnknown = true)
public class Device {

	public enum DeviceType {
		MASTER, SLAVE
	}

	public static class Listener {

		@PreUpdate
		@PrePersist
		public void update(Device d) {
			Logger.debug(" ----------------> Device aggiornato: " + d);
		}

	}

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	@Column(unique = true, nullable = false)
	private String matricola;

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	@Column(scale = 10, precision = 12)
	private BigDecimal lat;

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	@Column(scale = 10, precision = 12)
	private BigDecimal lon;

	@ManyToOne
	@JsonView(DeviceDriven.class)
	private Site site;

	@OneToOne(fetch = FetchType.EAGER)
	@JsonView({ DeviceDriven.class, SiteDriven.class })
	private Concentrator concentrator;

	@OneToMany(cascade = CascadeType.MERGE)
	@JoinColumn(name = "nodeDest_id")
	@NotAudited
	private Set<DynamicData> ddDest = new HashSet<DynamicData>();

	@OneToMany(cascade = CascadeType.MERGE)
	@JoinColumn(name = "nodeDest_id")
	@NotAudited
	private Set<StaticData> sdDest = new HashSet<StaticData>();

	@OneToMany(cascade = CascadeType.MERGE)
	@JoinColumn(name = "nodeRange_id")
	@NotAudited
	private Set<DynamicData> ddRange = new HashSet<DynamicData>();

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	private Integer batteryIndicator;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Device other = (Device) obj;
		if (this.getMatricola() == null) {
			if (other.getMatricola() != null) {
				return false;
			}
		} else if (!this.getMatricola().equals(other.getMatricola())) {
			return false;
		}
		return true;
	}

	public Integer getBatteryIndicator() {
		return this.batteryIndicator;
	}

	public Concentrator getConcentrator() {
		return this.concentrator;
	}

	public Set<DynamicData> getDdDest() {
		return this.ddDest;
	}

	public Set<DynamicData> getDdRange() {
		return this.ddRange;
	}

	public Long getId() {
		return this.id;
	}

	public BigDecimal getLat() {
		return this.lat;
	}

	public BigDecimal getLon() {
		return this.lon;
	}

	public String getMatricola() {
		return this.matricola;
	}

	public Set<StaticData> getSdDest() {
		return this.sdDest;
	}

	public Site getSite() {
		return this.site;
	}

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	public DeviceType getType() {
		return this.concentrator != null ? DeviceType.MASTER : DeviceType.SLAVE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ (this.getMatricola() == null ? 0 : this.getMatricola()
						.hashCode());
		return result;
	}

	public void setBatteryIndicator(Integer batteryIndicator) {
		this.batteryIndicator = batteryIndicator;
	}

	public void setConcentrator(Concentrator concentrator) {
		this.concentrator = concentrator;
	}

	public void setDdDest(Set<DynamicData> ddDest) {
		this.ddDest = ddDest;
	}

	public void setDdRange(Set<DynamicData> ddRange) {
		this.ddRange = ddRange;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public void setLon(BigDecimal lon) {
		this.lon = lon;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	public void setSdDest(Set<StaticData> sdDest) {
		this.sdDest = sdDest;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	@Override
	public String toString() {
		return "Device [id=" + this.id + ", matricola=" + this.matricola
				+ ", type=" + this.getType() + "]";
	}

}
