package model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;

import model.Views.DeviceDriven;
import model.Views.SiteDriven;

import org.hibernate.envers.Audited;

import play.Logger;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Audited
@EntityListeners(Site.Listener.class)
public class Site {

	public static class Listener {

		@PreUpdate
		@PrePersist
		public void update(Site s) {
			Logger.debug("---------> listener site " + s.getId());
		}

	}

	public enum SiteStatus {
		CONFIGURATION, RECORDING, STOP
	}

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	private String name;

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	@Column(scale = 10, precision = 12)
	private BigDecimal lat;

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	@Column(scale = 10, precision = 12)
	private BigDecimal lon;

	// @JsonManagedReference("site")
	@OneToMany(cascade = CascadeType.MERGE)
	@JoinColumn(name = "site_id")
	@OrderBy(value = "id")
	@JsonView(SiteDriven.class)
	private Set<Device> devices = new HashSet<>();

	@JsonView(SiteDriven.class)
	private Integer zoom = 11;
	@JsonView(SiteDriven.class)
	private String mapTypeId = "roadmap";

	@JsonView(SiteDriven.class)
	private Integer landslideZoom = 19;
	@JsonView(SiteDriven.class)
	private String landslideMapTypeId = "satellite";

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	@Enumerated(EnumType.STRING)
	private SiteStatus status = SiteStatus.CONFIGURATION;

	public Set<Device> getDevices() {
		return this.devices;
	}

	public Long getId() {
		return this.id;
	}

	public String getLandslideMapTypeId() {
		return this.landslideMapTypeId;
	}

	public Integer getLandslideZoom() {
		return this.landslideZoom;
	}

	public BigDecimal getLat() {
		return this.lat;
	}

	public BigDecimal getLon() {
		return this.lon;
	}

	public String getMapTypeId() {
		return this.mapTypeId;
	}

	@Transient
	public List<Device> getMasters() {
		List<Device> masters = new ArrayList<Device>();
		for (Device device : this.getDevices()) {
			if (device.getConcentrator() != null) {
				masters.add(device);
			}
		}
		return masters;
	}

	public String getName() {
		return this.name;
	}

	public SiteStatus getStatus() {
		return this.status;
	}

	public Integer getZoom() {
		return this.zoom;
	}

	public void setDevices(Set<Device> devices) {
		this.devices = devices;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLandslideMapTypeId(String landslideMapTypeId) {
		this.landslideMapTypeId = landslideMapTypeId;
	}

	public void setLandslideZoom(Integer landslideZoom) {
		this.landslideZoom = landslideZoom;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public void setLon(BigDecimal lon) {
		this.lon = lon;
	}

	public void setMapTypeId(String mapTypeId) {
		this.mapTypeId = mapTypeId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(SiteStatus status) {
		this.status = status;
	}

	public void setZoom(Integer zoom) {
		this.zoom = zoom;
	}

}
