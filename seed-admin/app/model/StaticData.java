package model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import model.Views.ValuesDriven;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class StaticData {

	public enum DataType {
		INCLINAZIONE, ACCELEROMETRO, GIROSCOPIO, CAMPOMAGNETICO
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String rawData;

	@Column(nullable = false)
	private Integer numTriples;

	@JsonView({ ValuesDriven.class })
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	@JsonView({ ValuesDriven.class })
	@Enumerated(EnumType.STRING)
	private DataType dataType = DataType.INCLINAZIONE;

	@ManyToOne
	private Device nodeDest;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "staticData_id")
	private Set<StaticValues> values = new HashSet<StaticValues>();

	public Date getData() {
		return this.data;
	}

	public DataType getDataType() {
		return this.dataType;
	}

	public Long getId() {
		return this.id;
	}

	public Device getNodeDest() {
		return this.nodeDest;
	}

	public Integer getNumTriples() {
		return this.numTriples;
	}

	public String getRawData() {
		return this.rawData;
	}

	public Set<StaticValues> getValues() {
		return this.values;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNodeDest(Device nodeDest) {
		this.nodeDest = nodeDest;
	}

	public void setNumTriples(Integer numTriples) {
		this.numTriples = numTriples;
	}

	public void setRawData(String rawData) {
		this.rawData = rawData;
	}

	public void setValues(Set<StaticValues> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return this.rawData;
	}
}
