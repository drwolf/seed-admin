package model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import model.Views.ValuesDriven;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class DynamicData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String rawData;

	@Column(nullable = false)
	private Integer numSamples;

	@JsonView({ ValuesDriven.class })
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	@ManyToOne
	private Device nodeDest;

	@ManyToOne
	private Device nodeRange;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "dynamicData_id")
	private Set<DynamicValues> values = new HashSet<DynamicValues>();

	public Date getData() {
		return this.data;
	}

	public Long getId() {
		return this.id;
	}

	public Device getNodeDest() {
		return this.nodeDest;
	}

	public Device getNodeRange() {
		return this.nodeRange;
	}

	public Integer getNumSamples() {
		return this.numSamples;
	}

	public String getRawData() {
		return this.rawData;
	}

	public Set<DynamicValues> getValues() {
		return this.values;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNodeDest(Device nodeDest) {
		this.nodeDest = nodeDest;
	}

	public void setNodeRange(Device nodeRange) {
		this.nodeRange = nodeRange;
	}

	public void setNumSamples(Integer numSamples) {
		this.numSamples = numSamples;
	}

	public void setRawData(String rawData) {
		this.rawData = rawData;
	}

	public void setValues(Set<DynamicValues> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return this.rawData;
	}
}
