package model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import model.Views.ValuesDriven;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class DynamicValues {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonView({ ValuesDriven.class })
	@Column(scale = 10, precision = 19)
	private BigDecimal ranging;

	@JsonView({ ValuesDriven.class })
	@ManyToOne
	private DynamicData dynamicData;

	public DynamicData getDynamicData() {
		return this.dynamicData;
	}

	public Long getId() {
		return this.id;
	}

	public BigDecimal getRanging() {
		return this.ranging;
	}

	public void setDynamicData(DynamicData dynamicData) {
		this.dynamicData = dynamicData;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRanging(BigDecimal ranging) {
		this.ranging = ranging;
	}

}
