package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import model.Views.DeviceDriven;
import model.Views.SiteDriven;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Audited
@JsonIgnoreProperties(ignoreUnknown = true)
public class Concentrator {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonView({ DeviceDriven.class, SiteDriven.class })
	private Long id;

	@Column(unique = true, nullable = false)
	@JsonView({ DeviceDriven.class, SiteDriven.class })
	private String matricola;

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	private String address;

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	private String username;

	@JsonView({ DeviceDriven.class, SiteDriven.class })
	private String password;

	public String getAddress() {
		return this.address;
	}

	public Long getId() {
		return this.id;
	}

	public String getMatricola() {
		return this.matricola;
	}

	public String getPassword() {
		return this.password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
