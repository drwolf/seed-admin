package model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import model.Views.ValuesDriven;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class StaticValues {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonView({ ValuesDriven.class })
	@Column(scale = 10, precision = 19)
	private BigDecimal valueX;

	@JsonView({ ValuesDriven.class })
	@Column(scale = 10, precision = 19)
	private BigDecimal valueY;

	@JsonView({ ValuesDriven.class })
	@Column(scale = 10, precision = 19)
	private BigDecimal valueZ;

	@JsonView({ ValuesDriven.class })
	@ManyToOne
	private StaticData staticData;

	public Long getId() {
		return this.id;
	}

	public StaticData getStaticData() {
		return this.staticData;
	}

	public BigDecimal getValueX() {
		return this.valueX;
	}

	public BigDecimal getValueY() {
		return this.valueY;
	}

	public BigDecimal getValueZ() {
		return this.valueZ;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setStaticData(StaticData staticData) {
		this.staticData = staticData;
	}

	public void setValueX(BigDecimal valueX) {
		this.valueX = valueX;
	}

	public void setValueY(BigDecimal valueY) {
		this.valueY = valueY;
	}

	public void setValueZ(BigDecimal valueZ) {
		this.valueZ = valueZ;
	}
}
