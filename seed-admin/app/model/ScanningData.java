package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class ScanningData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String rawData;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	@Column
	private Long nodeStartId;

	@Column
	private Long nodeEndId;

	@Column
	private String result;

	@ManyToOne
	private Device nodeDest;

	public Date getData() {
		return this.data;
	}

	public Long getId() {
		return this.id;
	}

	public Device getNodeDest() {
		return this.nodeDest;
	}

	public Long getNodeEndId() {
		return this.nodeEndId;
	}

	public Long getNodeStartId() {
		return this.nodeStartId;
	}

	public String getRawData() {
		return this.rawData;
	}

	public String getResult() {
		return this.result;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNodeDest(Device nodeDest) {
		this.nodeDest = nodeDest;
	}

	public void setNodeEndId(Long nodeEndId) {
		this.nodeEndId = nodeEndId;
	}

	public void setNodeStartId(Long nodeStartId) {
		this.nodeStartId = nodeStartId;
	}

	public void setRawData(String rawData) {
		this.rawData = rawData;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return this.rawData;
	}
}
