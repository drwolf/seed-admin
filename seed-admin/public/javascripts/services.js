seedApp.factory('authenticationService', ['$resource', '$rootScope', '$location',
	function($resource, $rootScope, $location){
	
	return {
		
		retrieveCurrentUser: function(){
			$rootScope.identity = $resource('/seed/currentuser').get();
		},
		
		logout: function(){
			var from = $location.path();
			return $resource('/seed/logout').get(function(){
				$rootScope.identity = {};
				$location.url('/login?from='+from);
			});
		}	
	}

}]);


seedApp.factory('Site', ['$resource', function($resource){	
	return $resource('/seed/site/:siteId',{siteId:'@id'}, {
       update: {method:'POST'}
      });
}]);

seedApp.factory('Device', ['$resource', function($resource){	
	return $resource('/seed/device/:deviceId',{deviceId:'@id'}, {
       update: {method:'POST'}
      });
}]);

seedApp.factory('Concentrator', ['$resource', function($resource){	
	return $resource('/seed/concentrator/:concentratorId',{concentratorId:'@id'}, {
       update: {method:'POST'}
      });
}]);

seedApp.factory('User', ['$resource', function($resource){	
	return $resource('/seed/user/:userId',{userId:'@id'}, {
       update: {method:'POST'}
      });
}]);

seedApp.factory('alertService', ['$rootScope', function($rootScope) {

    var alertService = {};
	
	alertService.setGlobalTitle = function(title){
		$rootScope.globalTitle = title;
	}

    return alertService;

}]);



seedApp.factory('validationService', ['$rootScope', function($rootScope) {

    var validationService = {};
	
	validationService.floatRegex = '/^[0-9]+([.][0-9]+)?$/';
	
	validationService.toFloat = function(string){
		return parseFloat((''+string).replace(',','.'));
	}

    return validationService;

  }
]);


seedApp.factory('playSession', ['$browser', function($browser) {
    return function(){
      // read Play session cookie
      var rawCookie = $browser.cookies().PLAY_SESSION;
      var session = {};
      if (rawCookie){
        var rawData = rawCookie.substring(rawCookie.indexOf('-') + 1, rawCookie.length - 1);
        _.each(rawData.split("&"), function(rawPair) {
          var pair = rawPair.split('=');
          session[pair[0]] = pair[1];
        });
      }
      return session;
    };
}]);

