//
// Sites Controller
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
seedApp
    .controller('SitesController', ['$scope', 'Site', 'ngTableParams', 'alertService', '$filter', 'validationService', 'growl',
        function ($scope, Site, ngTableParams, alertService, $filter, validationService, growl) {
            
    function newEmptyDevice(){
        $scope.selected = new Site();
        $scope.stored = false;
    }
    
    function undoEditing(did, dindex){
        var oldDevice = Site.get({siteId:did}, function(device){
            devices[dindex] = oldDevice;
            $scope.selected = oldDevice;
            $scope.tableParams.reload();
            //growl.info('Annullate le modifiche per il device: ' + oldDevice.name);
            newEmptyDevice();
        });
    }
    
    alertService.setGlobalTitle(' : Lista dei siti monitorati');
    
    //$scope.validator = validationService;
    
    var devices = [];
    
    newEmptyDevice();
    
    $scope.tableParams = new ngTableParams({
        page: 1,           // show first page
        count: 10,           // count per page
        sorting: {
            id: 'asc'     // initial sorting
        }
    }, {
        total: 0, // length of data
        
        getData: function($defer, params) {
            // per evitare che venga fatta la chiamata ad ogni cambio di pagina
            if(!_(devices).isEmpty()){
                devices = params.sorting() ?
                            $filter('orderBy')(devices, params.orderBy()) :
                               devices;
                params.total(devices.length);
                $defer.resolve(devices.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }else{
                devices = Site.query(function(result){
                    devices = $filter('orderBy')(result, params.orderBy());
                    params.total(result.length);
                    $defer.resolve(devices.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
            }
        }
    });
    
    
    $scope.save = function(){
        
        //alertService.empty();
        
        if(!$scope.selected.id){
            $scope.selected.$save(function(result){
                growl.success('Sito salvato correttamente');
                devices.push(result);

                newEmptyDevice();

                $scope.tableParams.reload();
            }, function(error){
                growl.error('Errore nel salvataggio del sito: ' + error.data);
            });
        }else{
            $scope.selected.$update(function(result){
                growl.success('Device aggiornato correttamente');
                newEmptyDevice();
            }, function(error){
                growl.error('Errore nell\'aggiornamento del sito: ' + error.data);
                undoEditing($scope.selected.id, selectedIndex);
            });
        }
    }
    
    $scope.remove = function(item){
        
        //alertService.empty();
        
        $scope.selected.$delete(function(result){
                growl.success('Device "'+ $scope.selected.name +'" eliminato correttamente');
                
                devices = _.filter(devices,function(dev){
                    if(dev.id != $scope.selected.id){
                        return true;
                    }else{
                        return false;
                    }
                });

                newEmptyDevice();
                $scope.tableParams.reload();
            }, function(error){
                growl.error('Errore nel rimuovere il device: ' + error.data);
            });
    }
    
    $scope.select = function(item){
        //alertService.empty();
        selectedIndex = _.indexOf(devices, item);
        $scope.selected = item;
        $scope.stored = true;
    }
    
    $scope.unselect = function(){
        //alertService.empty();
        var did = $scope.selected.id;
        undoEditing(did, selectedIndex);
        newEmptyDevice();
    }
    
    $scope.stored = false;

    
}]);
