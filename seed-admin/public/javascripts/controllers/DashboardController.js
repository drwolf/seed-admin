//
// Main controller
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
seedApp.controller('DashboardController', ['$scope', 'authenticationService',
    function ($scope, authenticationService) {

        /* VERSIONE DEL SOFTWARE */
        $scope.version = '0.6.2';

        $scope.nomeTest = 'Valore di test';

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        authenticationService.retrieveCurrentUser();

        $scope.logout = function(){
            authenticationService.logout();
        };

}]);
