//
// Login controller
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
seedApp.controller('LoginController', ['$scope', '$http', 'authenticationService', 'alertService', '$route', '$location', '$rootScope', 'growl',
    function ($scope, $http, authenticationService, alertService, $route, $location, $rootScope, growl) {
        
        $scope.authenticate = function(){
            $http({method: 'POST', url: '/seed/login', data: {username: $scope.username, password: $scope.password}})
                .success(function(data){
                    var from = $route.current.params.from;
                    $location.path(from);
                    //
                    authenticationService.retrieveCurrentUser();
                    //
                    growl.success('Bentornato ' + '"'+$scope.username+'"');
                })
                .error(function(data, status, headers, config) {
                    growl.error('Errore di autenticazione: ' + data);
                });
        }
        
        $scope.username;

        $scope.password;

}]);
