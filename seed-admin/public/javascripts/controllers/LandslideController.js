//
// Landslide Controller
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
seedApp
    .controller('LandslideController',
        ['$scope', 'Site', '$routeParams', 'Device', 'ngTableParams', 'alertService', 'validationService', '$http', '$filter', '$route', 'growl',
              function ($scope, Site, $routeParams, Device, ngTableParams, alertService, validationService, $http, $filter, $route, growl) {

    function getMarkerByDevId(deviceId){
        return _.find(markers, function(marker){
            if(deviceId == marker.deviceId){
                return true;
            }
            return false;
        });
    }


    function closeAllMarkerInfos(deviceId){
        _.each(markers, function(marker){
            marker.infoWindow.setContent(marker.title);
            if(marker.deviceId!=deviceId){
                marker.infoWindow.close($scope.gmap, marker);
            }
        });
    }


    function emptyLines(line){
        // faccio le differenze e tolgo tutti gli altri
        if(line){
            childrenLines = _.difference(childrenLines, line);
            childrenIds = _.difference(childrenIds, line.targetId);
        };
        while(childrenLines.length > 0) {
            var l = childrenLines.pop();
            l.setMap(null);
        }
        while(childrenIds.length > 0) {
            childrenIds.pop();
        }

        // rimetto i valori che non vogliano sia cancellati
        if(line){
            childrenLines.push(line);
            childrenIds.push(line.targetId);
        }
    };

    function resetConnectionLines(line){
        _.each(childrenLines, function(line){
            line.setOptions({
                strokeOpacity: 0.5
            });
        });
    };


    function handleDeviceChanges(newVal, oldVal, key){
        if(newVal !== oldVal){
            console.log($scope.site.devices[key].matricola + ' LON was changed from: '+ oldVal + ' to: ' + newVal);
            $scope.dirty = true;
            closeAllMarkerInfos($scope.site.devices[key].id);
            alertService.setGlobalTitle(' : ' + $scope.site.name + ' *');
        }
    }

    function resetSymbols(excludedIds){
        _.each($scope.site.devices, function(device){
            if(!excludedIds || !_.contains(excludedIds,device.id)){
                var marker = getMarkerByDevId(device.id);
                drawSymbol(marker, device.type);
            }
        });
    }

    function drawSymbol(marker, devType, status){
        var fc = '#FFF';
        var sw = 3;
        var magnifier = 1;
        if(status == 'SELECTED'){
            //fc = '#d0e9c6';
            fc='#75be58';
            magnifier = 1.7;
        }else if(status == 'CHILD'){
            //fc = '#faf2cc';
            fc = '#f4e28e';
        }

        marker.setOptions({
            icon: {
                  path: devType == 'SLAVE' ? google.maps.SymbolPath.CIRCLE : google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                  scale: devType == 'SLAVE' ? 8*magnifier : 6*magnifier,
                  strokeColor: '#F00',
                  fillColor: fc,
                  fillOpacity: 1,
                  strokeWeight: sw * magnifier,
                  strokeOpacity: 0.8
                }
            });
    }


    function formatStaticData(data){
        var out = '<h4>Valori Statici per il rilevatore '+ $scope.selected.matricola+ '</h4>';
        out = out + '<table class="table table-condensed table-hover infoWindow" style="height:200px">'
        + '<tr><th>Data</th><th>Tipo</th><th>X</th><th>Y</th><th>Z</th></tr>';
        _.each(data, function(d){
            var date = sutils.convertTimestamp(d.staticData.data);
            out = out + '<tr><td>'+ date +'</td>';
            out = out + '<td>'+ d.staticData.dataType +'</td>';
            out = out + '<td>'+ d.valueX +'</td>';
            out = out + '<td>'+ d.valueY +'</td>';
            out = out + '<td>'+ d.valueZ +'</td></tr>';
        });
        out = out + '</table>';
        return out;
    }


    function formatDynamicData(data){

        var dataToShow = [];
        _.each(data, function(d, index){
            var item = {};
            item.date = new Date(d.dynamicData.data);
            item.value = parseFloat(d.ranging);
            dataToShow.push(item);
        });

        $scope.selected.chartData = dataToShow;

        var out = '<h4>Valori Dinamici concentratore ' + $scope.selected.matricola + '</h4>';
        out = out + '<div id="info_' + $scope.selected.matricola + '" style="height:300px;width:300px"></div>';
        return out;
    }


    function drawGraph(){
        var item = $('#info_' + $scope.selected.matricola);
        var itemId = item[0].id

        MG.data_graphic({
            data: $scope.selected.chartData,
            x_accessor: "date",
            y_accessor: "value",
            width: 280,
            height: 280,
            target: '#' + itemId
        });
    }


    function drawLine(device, child){
        var segments = [];

        var marker = getMarkerByDevId(device.id);
        segments.push(marker.getPosition());

        var childMarker = getMarkerByDevId(child.id);
        segments.push(childMarker.getPosition());

        childMarker.infoWindow.open($scope.gmap, childMarker);
        drawSymbol(childMarker, child.type, 'CHILD');

        childrenIds.push(child.id);

        var line = new google.maps.Polyline({
            path: segments,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 0.5,
            strokeWeight: 5
        });
        line.setMap($scope.gmap);

        line.sourceId = device.id;
        line.targetId = child.id;

        childrenLines.push(line);

        google.maps.event.addListener(line, 'click', function() {
            device.loading = true;
            closeAllMarkerInfos();
            emptyLines(line)
            resetSymbols([device.id, child.id]);
            line.setOptions({strokeOpacity: 0.7});
                $http({method: 'GET', url: '/seed/device/'+device.id+'/dynamic/'+child.id}).
                    success(function(data, status, headers, config) {
                        //controllo che non sia cambiato il device selezionato
                        if(device.id == $scope.selected.id){
                            growl.info('Recuperati dati dinamici per il device \'' + device.matricola + '\'');

                            marker.infoWindow.setContent(formatDynamicData(data));
                            marker.infoWindow.open($scope.gmap, marker);

                            device.loading = false;
                        }
                    }).
                    error(function(data, status, headers, config) {
                      growl.error('Errore nell\'elaborazione dei dati dinamici per il device ' + device.matricola + ' : ' + data);
                      device.loading = false;
                    });

        });
    }


    $scope.unselect = function(){
        $scope.selected = null;
        closeAllMarkerInfos();
        emptyLines();
        resetSymbols();
    }

    $scope.isConfigured = function(){
        if($scope.site.devices && $scope.site.devices.length == 0){return false;}
        var dev = _.find($scope.site.devices, function(dev){
            return !dev.lat || !dev.lon;
        });
        if(dev){
            return false;
        }else{
            return true;
        }
    }

    $scope.isChild =function(device){
        return _.contains(childrenIds, device.id);
    }


    $scope.updateMap =function(){
        if(!_($scope.site).isUndefined() && !_($scope.gmap).isUndefined()){

            $scope.gmap.setMapTypeId(!_($scope.site.landslideMapTypeId).isUndefined() ? $scope.site.landslideMapTypeId : google.maps.MapTypeId.SATELLITE);
            $scope.gmap.setZoom(!_($scope.site.landslideZoom).isUndefined() ? $scope.site.landslideZoom : 19);

            emptyLines();

            _.each($scope.site.devices, function(dev){

                if(!_(dev.lat).isUndefined() && !_(dev.lon).isUndefined()){

                    if(_(getMarkerByDevId(dev.id)).isUndefined()) {
                        // marker non presente
                        var marker = new google.maps.Marker({
                            map: $scope.gmap,
                            position : {lat: validationService.toFloat(dev.lat), lng: validationService.toFloat(dev.lon)}
                            });
                        marker.setTitle(dev.matricola);
                        marker.deviceId = dev.id;
                        drawSymbol(marker, dev.type);

                        var infoWindow = new google.maps.InfoWindow();
                        infoWindow.setContent(dev.matricola);
                        marker.infoWindow = infoWindow;

                        // Listener per mostrare grafico
                        if(dev.type==='MASTER'){
                            google.maps.event.addListener(marker.infoWindow, 'domready', drawGraph);
                        }

                        markers.push(marker);

                        // Listener sull'oggetto marker che onclick chiama il metodo "$scope.select(devSelected)"
                        google.maps.event.addListener(marker, 'click', function() {
                            var devSelected = _.find($scope.site.devices, function(dev){
                                if(dev.id == marker.deviceId){
                                    return true;
                                }
                                return false;
                            });
                            $scope.select(devSelected);
                            $scope.$apply();
                        });

                    }else{
                        // marker già presente
                        var marker = getMarkerByDevId(dev.id);
                        marker.setPosition({lat: validationService.toFloat(dev.lat), lng: validationService.toFloat(dev.lon)});
                    }

                }
            });

        }

    }

    $scope.select = function(device){
        if($scope.selected){
            $scope.selected.loading = false;
        }

        resetSymbols();
        emptyLines();
        closeAllMarkerInfos();

        $scope.selected = device;
        var marker = getMarkerByDevId(device.id);

        if(!_(device.lat).isNull() && !_(device.lon).isNull() && marker){

            drawSymbol(marker, device.type, 'SELECTED');

            if(device.type=='MASTER'){
                device.loading = true;
                $http({method: 'GET', url: '/seed/device/'+device.id+'/children'}).
                    success(function(children, status, headers, config) {
                        //controllo che non sia cambiato il device selezionato
                        if(device.id == $scope.selected.id){
                            emptyLines();
                            _.each(children, function(child){
                                drawLine(device, child);
                            });

                            growl.info('Trovati ' + children.length + ' rilevatori collegati al concentratore \'' + device.matricola + '\'');
                            device.loading = false;
                        }
                    }).
                    error(function(data, status, headers, config) {
                      growl.error('Errore nell\'elaborazione dei devices collegati: ' + data);
                      device.loading = false;
                    });
            }else{
                device.loading = true;
                $http({method: 'GET', url: '/seed/device/'+device.id+'/static'}).
                    success(function(data, status, headers, config) {
                        //controllo che non sia cambiato il device selezionato
                        if(device.id == $scope.selected.id){
                            emptyLines();
                            var formattedOutput = formatStaticData(data);

                            marker.infoWindow.setContent(formattedOutput);
                            marker.infoWindow.open($scope.gmap, marker);

                            growl.info('Recuperati dati statici per il rilevatore \'' + device.matricola + '\'');
                            device.loading = false;
                        }
                    }).
                    error(function(data, status, headers, config) {
                      growl.error('Errore nell\'elaborazione dei dati statici per il rivelatore ' + device.matricola + ' : ' + data);
                      device.loading = false;
                    });
            }
        }
    }

    $scope.save = function(){
        $scope.site.$save(function(data){
            //$scope.site = data;
            //$scope.updateMap();
            //growl.success('Sito aggiornato correttamente');
            $route.reload();
        });

    }

    $scope.start = function(speed){
        console.log(speed);
        var oldStatus = $scope.site.status;
        $scope.start.loading = true;
        var params = {};
        params.speed = speed;
        $http({method: 'POST', url: '/seed/site/'+$scope.site.id+'/start', data: params}).
                success(function(data, status, headers, config) {
                    $scope.site.status = JSON.parse(data);
                    console.log('---> RECORDING');
                    growl.info('Inizio elaborazione');
                    $scope.start.loading = false;
                }).
                error(function(data, status, headers, config) {
                  growl.error('Errore nell\'elaborazione: ' + data);
                  $scope.site.status = oldStatus;
                  $scope.start.loading = false;
                });
    }

    $scope.stop = function(){
        var oldStatus = $scope.site.status;
        $scope.stop.loading = true;
        $http({method: 'POST', url: '/seed/site/'+$scope.site.id+'/stop'}).
                success(function(data, status, headers, config) {
                    $scope.site.status = JSON.parse(data);
                    console.log('---> STOP');
                    growl.info('Fine elaborazione');
                    $scope.stop.loading = false;
                }).
                error(function(data, status, headers, config) {
                  growl.error('Errore nell\'elaborazione: ' + data);
                  $scope.site.status = oldStatus;
                  $scope.stop.loading = false;
                });
    }

    var markers = [];

    var childrenLines = [];

    var childrenIds = [];

    var sid = $routeParams.siteId;

    $scope.validator = validationService;

    //alertService.empty();

    $scope.site = Site.get({siteId:sid}, function(site){

        if(!_($scope.gmap).isUndefined()){
            $scope.gmap.setCenter({lat: $scope.site.lat, lng: $scope.site.lon});
        }

        $scope.tableParams = new ngTableParams({
                page: 1,           // show first page
                count: 16,           // count per page
                sorting: {
                    type: 'asc'     // initial sorting
                }
            }, {
                total: $scope.site.devices.length,

                getData: function($defer, params) {
                    var orderedDevices = params.sorting() ?
                            $filter('orderBy')($scope.site.devices, params.orderBy()) :
                               $scope.site.devices;

                    // Aggiunta del listener dopo il riordinamento perchè altrimenti viene chiamato al caricamento della pagina
                    angular.forEach($scope.site.devices, function(value, key) {
                        $scope.$watch('site.devices['+key+'].lat', function (newVal, oldVal) {
                            handleDeviceChanges(newVal, oldVal, key);
                        },true);

                        $scope.$watch('site.devices['+key+'].lon', function (newVal, oldVal) {
                            handleDeviceChanges(newVal, oldVal, key);
                        },true);
                    });
                    ////
                    $defer.resolve(orderedDevices.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
        });

        alertService.setGlobalTitle(' : ' + site.name);

        $scope.updateMap();

        $scope.dirty = false;

    });


    $scope.map = {
        center: {
            latitude: 44,
            longitude: 11
        },
        zoom: 19,
        events: {
            tilesloaded: _.once(function (map) {
                // Listeners sull'oggtto map
                google.maps.event.addListener(map, 'maptypeid_changed', function() {
                    $scope.site.landslideMapTypeId = $scope.gmap.getMapTypeId();
                });

                google.maps.event.addListener(map, 'zoom_changed', function() {
                    $scope.site.landslideZoom = $scope.gmap.getZoom();
                });

                google.maps.event.addListener(map, 'center_changed', function() {
                    $scope.site.lat = $scope.gmap.getCenter().lat();
                    $scope.site.lon = $scope.gmap.getCenter().lng();
                });

                google.maps.event.addListener(map, 'click', function() {
                     $scope.$apply(function() {
                        $scope.unselect();
                    });
                });

                $scope.$apply(function() {
                    $scope.gmap = map;
                    if(!_($scope.site).isUndefined()){
                        if($scope.site.lat && $scope.site.lon){
                            $scope.gmap.setCenter({lat: $scope.site.lat, lng: $scope.site.lon});
                        }else{
                            if(navigator.geolocation) {
                                navigator.geolocation.getCurrentPosition(function(position) {
                                  var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                                  map.setCenter(pos);
                                }, function() {
                                  console.log('geolocation failed');
                                });
                            }
                        }
                    }

                    $scope.updateMap();
                });
            })
        },
        options: {
            mapTypeId : google.maps.MapTypeId.SATELLITE,
            scaleControl: true,
            streetViewControl: false
        }
    };


    $scope.status = 'Stop';


    $scope.$watch('selected.chart', function (oldVal, newVal) {

        console.log(newVal);



        });



}]);
