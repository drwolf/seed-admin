//
// Users Controller
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
seedApp
    .controller('UsersController', ['$scope', 'User', 'ngTableParams', '$routeParams', '$http', 'alertService', '$filter', 'growl',
        function ($scope, User, ngTableParams, $routeParams, $http, alertService, $filter, growl) {
            
    function newEmptyUser(){
        $scope.selected = new User();
        $scope.selected.active = true;
        $scope.stored = false;
    }
    
    function undoEditing(did, dindex){
        var oldDevice = User.get({userId:did}, function(device){
            users[dindex] = oldDevice;
            $scope.selected = oldDevice;
            $scope.tableParams.reload();
            //growl.info('Annullate le modifiche per il device: ' + oldDevice.matricola);
            newEmptyUser();
        });
    }
    
    alertService.setGlobalTitle(' : Lista degli utenti');
    
    var users = [];
    
    var selectedIndex;
    
    newEmptyUser();
    
    $scope.tableParams = new ngTableParams({
        page: 1,           // show first page
        count: 15,           // count per page
        sorting: {
            email: 'asc'     // initial sorting
        }
    }, {
        total: 0, // length of data
        
        getData: function($defer, params) {
            // per evitare che venga fatta la chiamata ad ogni cambio di pagina
            if(!_(users).isEmpty()){
                users = params.sorting() ?
                            $filter('orderBy')(users, params.orderBy()) :
                               users;
                params.total(users.length);
                $defer.resolve(users.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }else{
                users = User.query(function(result){
                    users = $filter('orderBy')(result, params.orderBy());
                    params.total(result.length);
                    $defer.resolve(users.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
            }
        }
    });
    
    
    $scope.save = function(){
        
        //alertService.empty();
        
        if(!$scope.selected.id){
            $scope.selected.$save(function(result){
                growl.success('User salvato correttamente');
                users.push(result);

                newEmptyUser();

                $scope.tableParams.reload();
            }, function(error){
                growl.error('Errore nel salvataggio del user: ' + error.data);
            });
        }else{
            $scope.selected.$update(function(result){
                growl.success('User aggiornato correttamente');
                newEmptyUser();
            }, function(error){
                growl.error('Errore nell\'aggiornamento del device: ' + error.data);
                undoEditing($scope.selected.id, selectedIndex);
            });
        }
    }
    
    $scope.remove = function(item){
        
        //alertService.empty();
        
        $scope.selected.$delete(function(result){
                growl.success('User "'+ $scope.selected.matricola +'" eliminato correttamente');
                
                users = _.filter(users,function(dev){
                    if(dev.id != $scope.selected.id){
                        return true;
                    }else{
                        return false;
                    }
                });

                newEmptyUser();
                $scope.tableParams.reload();
            }, function(error){
                growl.error('Errore nel rimuovere lo user: ' + error.data);
            });
    }
    
    $scope.select = function(item){
        //alertService.empty();
        selectedIndex = _.indexOf(users, item);
        $scope.selected = item;
        $scope.stored = true;
    }
    
    $scope.unselect = function(){
        //alertService.empty();
        var did = $scope.selected.id;
        undoEditing(did, selectedIndex);
        newEmptyUser();
    }
    
    $scope.stored = false;

}]);
