//
// Devices Controller
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
seedApp
    .controller('DevicesController', ['$scope', 'Device', 'ngTableParams', '$routeParams', '$http', 'alertService', '$filter', 'Concentrator', 'Site', 'growl',
        function ($scope, Device, ngTableParams, $routeParams, $http, alertService, $filter, Concentrator, Site, growl) {
    
    function newEmptyDevice(){
        $scope.device = new Device();
        $scope.concentrators = Concentrator.query({available:true});
        $scope.stored = false;
    }
    
    function undoEditing(did, dindex){
        var oldDevice = Device.get({deviceId:did}, function(device){
            devices[dindex] = oldDevice;
            $scope.device = oldDevice;
            $scope.tableParams.reload();
            //growl.info('Annullate le modifiche per il device: ' + oldDevice.matricola);
            newEmptyDevice();
        });
    }
    
    alertService.setGlobalTitle(' : Lista dei rivelatori disponibili');
    
    //alertService.empty();
    
    var devices = [];
    
    var selectedIndex;
    
    newEmptyDevice();
    
    $scope.tableParams = new ngTableParams({
        page: 1,           // show first page
        count: 15,           // count per page
        sorting: {
            matricola: 'asc'     // initial sorting
        }
    }, {
        total: 0, // length of data
        
        getData: function($defer, params) {
            // per evitare che venga fatta la chiamata ad ogni cambio di pagina
            if(!_(devices).isEmpty()){
                devices = params.sorting() ?
                            $filter('orderBy')(devices, params.orderBy()) :
                               devices;
                params.total(devices.length);
                $defer.resolve(devices.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }else{
                devices = Device.query(function(result){
                    devices = $filter('orderBy')(result, params.orderBy());
                    params.total(result.length);
                    $defer.resolve(devices.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
            }
        }
    });
    
    $scope.sites = Site.query();
    
    $scope.save = function(){
        
        //alertService.empty();
        
        if(!$scope.device.site){
            $scope.device.lat = null;
            $scope.device.lon = null;
        }
        
        if(!$scope.device.id){
            $scope.device.$save(function(result){
                growl.success('Device salvato correttamente');
                devices.push(result);

                newEmptyDevice();

                $scope.tableParams.reload();
            }, function(error){
                growl.error('Errore nel salvataggio del device: ' + error.data);
            });
        }else{
            $scope.device.$update(function(result){
                growl.success('Device aggiornato correttamente');
                newEmptyDevice();
            }, function(error){
                growl.error('Errore nell\'aggiornamento del device: ' + error.data);
                undoEditing($scope.device.id, selectedIndex);
            });
        }
    }
    
    $scope.remove = function(item){
        
        //alertService.empty();
        
        if(!$scope.device.site){
            $scope.device.lat = null;
            $scope.device.lon = null;
        }
        
        $scope.device.$delete(function(result){
                growl.success('Device "'+ $scope.device.matricola +'" eliminato correttamente');
                        
                devices = _.filter(devices,function(dev){
                    if(dev.id != $scope.device.id){
                        return true;
                    }else{
                        return false;
                    }
                });

                newEmptyDevice();
                $scope.tableParams.reload();
            }, function(error){
                growl.error('Errore nel rimuovere il device: ' + error.data);
            });
    }
    
    $scope.select = function(item){
        //alertService.empty();
        selectedIndex = _.indexOf(devices, item);
        $scope.device = item;
        $scope.stored = true;
        
        
        $scope.concentrators = Concentrator.query({deviceId:item.id, available:true}, function(result){
            if(item.concentrator){
                var concentrator = _.find($scope.concentrators, function(c){
                    if(c.id==item.concentrator.id){
                        return true;
                    }else{
                        return false;
                    }
                });
                item.concentrator = concentrator;
            }
        });
        
        if(item.site){
            var site = _.find($scope.sites, function(s){
                if(s.id==item.site.id){
                    return true;
                }else{
                    return false;
                }
            });
            item.site = site;
        }

    }
    
    $scope.unselect = function(){
        //alertService.empty();
        var did = $scope.device.id;
        undoEditing(did, selectedIndex);
        newEmptyDevice();
    }
    
    $scope.stored = false;

}]);
