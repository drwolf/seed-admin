var seedApp = angular.module('seedApp', 
		['ngRoute', 'ngResource', 'mgcrea.ngStrap', 'ui.bootstrap', 'ngTable', 'google-maps', 'angular-growl', 'ngAnimate']);

seedApp.config(['$routeProvider', 'growlProvider', function($routeProvider, growlProvider) {
	
	var authResolver = {
      auth: function($route, $q, $location, playSession){
        var defer = $q.defer();
        if (playSession().username){
          defer.resolve(true);
        } else {
			defer.reject("login required");
			$location.url('/login?from=' + ($route.current.$$route ? $route.current.$$route.originalPath : ''));
		}
        return defer.promise;
      }
    };

	$routeProvider
		.when('/login', {
			templateUrl: 'seed/assets/login.html',
			controller: 'LoginController'
		})
		.when('/sites', {
			templateUrl: 'seed/assets/sites.html',
			controller: 'SitesController',
			resolve: authResolver
		})
		.when('/devices', {
			templateUrl: 'seed/assets/devices.html',
			controller: 'DevicesController',
			resolve: authResolver
		})
		.when('/concentrators', {
			templateUrl: 'seed/assets/concentrators.html',
			controller: 'ConcentratorsController',
			resolve: authResolver
		})
		.when('/landslide/:siteId', {
			templateUrl: 'seed/assets/landslide.html',
			controller: 'LandslideController',
			resolve: authResolver
		})
		.when('/users', {
			templateUrl: 'seed/assets/users.html',
			controller: 'UsersController',
			resolve: authResolver
		})
		/*
		.when('/', {
			templateUrl: 'seed/assets/dashboard.html',
			controller: 'DashboardController',
			resolve: authResolver
		})
		.otherwise({
			templateUrl: 'seed/assets/dashboard.html',
			resolve: authResolver
		});
		*/
		.otherwise({
			templateUrl: 'seed/assets/sites.html',
			controller: 'SitesController',
			resolve: authResolver
		});
		
	 growlProvider.onlyUniqueMessages(false);
	 growlProvider.globalTimeToLive({success: 2000, error: -1, warning: 4000, info: 4000});
	 growlProvider.globalDisableCountDown(true);

}]);