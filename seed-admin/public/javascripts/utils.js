;var sutils = sutils || {};

sutils.convertTimestamp = function(timestamp){

    function normalize(d){
        d=''+d;
        if(d && d.length<2){
            d = '0'+d;
        }
        return d;
    }

    if(!timestamp){return "";}

    var date = new Date(timestamp);

    var year = date.getUTCFullYear();
    var month = date.getMonth()+1;
    var day = date.getDate();

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    var time = '';
    if(!(hours=='00' && minutes=='00' && seconds=='00')){time = ' ' + normalize(hours) + ':' + normalize(minutes);}

    return normalize(day) + '-' + normalize(month) + '-' + year + time;
}
