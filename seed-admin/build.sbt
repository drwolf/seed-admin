name := """seed-admin"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJpa,
  cache,
  javaWs,
  "mysql" % "mysql-connector-java" % "5.1.21",
  "org.hibernate" % "hibernate-entitymanager" % "4.2.2.Final",
  "org.hibernate" % "hibernate-envers" % "4.2.2.Final",
  "commons-net" % "commons-net" % "3.1",
  "commons-io" % "commons-io" % "2.4",
  "net.sf.opencsv" % "opencsv" % "2.0"
)
